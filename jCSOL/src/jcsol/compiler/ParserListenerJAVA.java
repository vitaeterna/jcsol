package jcsol.compiler;

import java.util.ArrayList;
import java.util.Collections;

import jcsol.compiler.antlr4.CsolBaseListener;
import jcsol.compiler.antlr4.CsolParser.AssignmentContext;
import jcsol.compiler.antlr4.CsolParser.ConditionContext;
import jcsol.compiler.antlr4.CsolParser.CsolProgramContext;
import jcsol.compiler.antlr4.CsolParser.DeclarationContext;
import jcsol.compiler.antlr4.CsolParser.ForkContext;
import jcsol.compiler.antlr4.CsolParser.ForkstmtContext;
import jcsol.compiler.antlr4.CsolParser.IfstmtContext;
import jcsol.compiler.antlr4.CsolParser.JavaContext;
import jcsol.compiler.antlr4.CsolParser.LoopContext;
import jcsol.compiler.antlr4.CsolParser.MethodCallContext;
import jcsol.compiler.antlr4.CsolParser.PauseContext;
import jcsol.compiler.antlr4.CsolParser.SkipContext;
import jcsol.data.ControlFlowGraph;
import jcsol.data.ControlFlowGraphNode;
import jcsol.data.ControlFlowGraphNode.Type;

public class ParserListenerJAVA extends CsolBaseListener
{
    private ControlFlowGraph                cfg                  = null;
    private ArrayList<ControlFlowGraphNode> vertices             = null;

    private StringBuilder                   generatedCode        = null;

    private String                          tab                  = null;
    private int                             tabDepth             = 1;

    private ArrayList<Integer>              threadStack          = null;
    private ArrayList<ControlFlowGraphNode> forkStack            = null;
    private int                             threadCount          = 1;
    private int                             temp                 = 0;

    private ArrayList<Integer>              ifStack              = null;

    private boolean                         assignment           = false;
    private String                          assignmentIdentifier = null;
    private String                          assignmentType       = null;

    private boolean                         emptyLine            = true;
    private boolean                         fork                 = false;
    private boolean                         updatePrediction     = false;

    public ParserListenerJAVA(ControlFlowGraph cfg)
    {
        this.cfg = cfg;

        vertices = new ArrayList<ControlFlowGraphNode>();

        generatedCode = new StringBuilder();

        tab = "    ";

        threadStack = new ArrayList<Integer>();
        ifStack = new ArrayList<Integer>();
        forkStack = new ArrayList<ControlFlowGraphNode>();

        ControlFlowGraphNode.resetIdCounter();
    }

    private void update_EmptyLine_Prediction_Fork(int id)
    {
        if(emptyLine)
        {
            if(vertices.get(vertices.size() - 1).getType() != Type.DECLARATION)
            {
                generatedCode.append("\n");
            }
        }
        else
        {
            emptyLine = true;
        }

        if(updatePrediction)
        {
            generateTabs();
            generatedCode.append("Register.setPrediction(Thread.currentThread(), this, " + vertices.get(vertices.size() - 1).getId() + ");\n");
            generatedCode.append("\n");

            updatePrediction = false;
        }

        if(fork)
        {
            temp = threadStack.get(threadStack.size() - 1);

            if(temp % 2 == 1)
            {
                threadStack.set(threadStack.size() - 1, temp + 1);
            }
            else
            {
                threadStack.set(threadStack.size() - 1, temp - 1);
            }

            generateTabs();
            generatedCode.append("Task task" + temp + " = new Task(cloneAndAddAncestor(Thread.currentThread()), " + id + ")\n");
            generateTabs();
            generatedCode.append("{\n");

            tabDepth++;

            generateTabs();
            generatedCode.append("@Override\n");
            generateTabs();
            generatedCode.append("public void run()\n");
            generateTabs();
            generatedCode.append("{\n");

            tabDepth++;

            fork = false;
        }
    }

    @Override
    public void enterCsolProgram(CsolProgramContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.START));

        generatedCode.append("package jcsol.runtime.generated;\n");
        generatedCode.append("\n");
        generatedCode.append("import java.util.ArrayList;\n");
        generatedCode.append("\n");
        generatedCode.append("import jcsol.runtime.schedule.Register;\n");
        generatedCode.append("import jcsol.runtime.schedule.Task;\n");

        ArrayList<String> scImports = new ArrayList<String>();

        for(ControlFlowGraphNode vertex : cfg.getVertices())
        {
            if(vertex.getType().equals(Type.DECLARATION))
            {
                if(!scImports.contains(vertex.getSCClass()))
                {
                    scImports.add(vertex.getSCClass());
                }
            }
        }

        Collections.sort(scImports, String.CASE_INSENSITIVE_ORDER);

        for(String scClass : scImports)
        {
            generatedCode.append("import jcsol.runtime.sharedClasses." + scClass + ";\n");
        }

        generatedCode.append("\n");
        generatedCode.append("public class GeneratedCode\n");
        generatedCode.append("{\n");
        generateTabs();
        generatedCode.append("public void execute()\n");
        generateTabs();
        generatedCode.append("{\n");

        tabDepth++;

        generateTabs();
        generatedCode.append("ArrayList<Thread> ancestors = new ArrayList<Thread>();\n");
        generateTabs();
        generatedCode.append("ancestors.add(Thread.currentThread());\n");

        for(ControlFlowGraphNode vertex : cfg.getVertices())
        {
            if(vertex.getType().equals(Type.DECLARATION))
            {
                for(String identifier : vertex.getIdentifiers())
                {
                    generatedCode.append("\n");
                    generateTabs();
                    generatedCode.append(vertex.getSCClass() + " " + identifier + " = new " + vertex.getSCClass() + "();\n");
                    generateTabs();
                    generatedCode.append(identifier + ".setIdentifier(\"" + identifier + "\");\n");
                    generateTabs();
                    generatedCode.append("Register.addCSO(\"" + identifier + "\", " + identifier + ");\n");
                }
            }
        }

        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Task task0 = new Task(ancestors, " + vertices.get(vertices.size() - 1).getId() + ")\n");
        generateTabs();
        generatedCode.append("{\n");

        tabDepth++;

        generateTabs();
        generatedCode.append("@Override\n");
        generateTabs();
        generatedCode.append("public void run()\n");
        generateTabs();
        generatedCode.append("{");

        tabDepth++;

        super.enterCsolProgram(ctx);
    }

    @Override
    public void exitCsolProgram(CsolProgramContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.STOP));

        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Register.notifyTermination(Thread.currentThread(), this, getChildrenAlive());\n");

        tabDepth--;

        generateTabs();
        generatedCode.append("}\n");

        tabDepth--;

        generateTabs();
        generatedCode.append("};\n");
        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Thread thread0 = new Thread(task0);\n");
        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Register.createThread(thread0, task0);\n");
        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Register.joinThread(thread0);\n");
        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Register.terminate();\n");

        tabDepth--;

        generateTabs();
        generatedCode.append("}\n");
        generatedCode.append("}");

        super.exitCsolProgram(ctx);
    }

    @Override
    public void enterJava(JavaContext ctx)
    {
        String[] javaCodeLines = ctx.getText().substring(ctx.getText().indexOf("{") + 1, ctx.getText().indexOf("}")).trim().split("\n");

        if(emptyLine)
        {
            generatedCode.append("\n");
        }
        else
        {
            emptyLine = true;
        }

        for(int index = 0; index < javaCodeLines.length; index++)
        {
            generateTabs();
            generatedCode.append(javaCodeLines[index].trim());

            if(index < javaCodeLines.length - 1)
            {
                generatedCode.append("\n");
            }
        }

        super.enterJava(ctx);
    }

    @Override
    public void enterMethodCall(MethodCallContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.CALL));

        if(!assignment)
        {
            update_EmptyLine_Prediction_Fork(vertices.get(vertices.size() - 1).getId());
        }

        generateTabs();

        String sharedAccess = ctx.IDENTIFIER(0).getText() + ctx.DOT().getText() + "sharedAccess(Thread.currentThread(), getAncestors(), " + cfg.getVertices().get(vertices.get(vertices.size() - 1).getId()).inWhile() + ", \"" + ctx.IDENTIFIER(1).getText() + "\"";

        if(ctx.parameters().getText().equals(""))
        {
            sharedAccess += ")";
        }
        else
        {
            sharedAccess += ", " + ctx.parameters().getText() + ")";
        }

        if(assignment)
        {
            generatedCode.append(assignmentType + " " + assignmentIdentifier + " = (" + assignmentType + ")" + sharedAccess + ";\n");

            assignment = false;
        }
        else
        {
            generatedCode.append(sharedAccess + ";\n");
        }

        super.enterMethodCall(ctx);
    }

    @Override
    public void enterDeclaration(DeclarationContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.DECLARATION));

        update_EmptyLine_Prediction_Fork(vertices.get(vertices.size() - 1).getId());

        super.enterDeclaration(ctx);
    }

    @Override
    public void enterSkip(SkipContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.SKIP));

        update_EmptyLine_Prediction_Fork(vertices.get(vertices.size() - 1).getId());

        generateTabs();
        generatedCode.append("//SKIP\n");

        super.enterSkip(ctx);
    }

    @Override
    public void enterPause(PauseContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.PAUSE));

        if(emptyLine)
        {
            generatedCode.append("\n");
        }
        else
        {
            emptyLine = true;
        }

        generateTabs();
        generatedCode.append("Register.pause(Thread.currentThread(), this, " + vertices.get(vertices.size() - 1).getId() + ");\n");

        super.enterPause(ctx);
    }

    @Override
    public void enterAssignment(AssignmentContext ctx)
    {
        assignment = true;

        if(ctx.type().BOOL() != null)
        {
            assignmentType = "boolean";
        }
        else if(ctx.type().INT() != null)
        {
            assignmentType = "int";
        }

        assignmentIdentifier = ctx.IDENTIFIER().getText();

        vertices.add(new ControlFlowGraphNode(ControlFlowGraphNode.Type.ASSIGN));

        update_EmptyLine_Prediction_Fork(vertices.get(vertices.size() - 1).getId());

        super.enterAssignment(ctx);
    }

    @Override
    public void enterFork(ForkContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.FORK));

        update_EmptyLine_Prediction_Fork(vertices.get(vertices.size() - 1).getId());

        forkStack.add(vertices.get(vertices.size() - 1));

        threadStack.add(threadCount);

        threadCount += 2;

        super.enterFork(ctx);
    }

    @Override
    public void exitFork(ForkContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.JOIN));

        temp = threadStack.get(threadStack.size() - 1);

        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Thread thread" + temp + " = new Thread(task" + temp + ");\n");
        generateTabs();
        generatedCode.append("Thread thread" + (temp + 1) + " = new Thread(task" + (temp + 1) + ");\n");
        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Register.createThreads(Thread.currentThread(), thread" + temp + ", thread" + (temp + 1) + ", this, task" + temp + ", task" + (temp + 1) + ", " + vertices.get(vertices.size() - 1).getId() + ");\n");
        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Register.joinThreads(thread" + temp + ", thread" + (temp + 1) + ");\n");

        threadStack.remove(threadStack.size() - 1);
        forkStack.remove(forkStack.size() - 1);

        super.exitFork(ctx);
    }

    @Override
    public void enterForkstmt(ForkstmtContext ctx)
    {
        fork = true;

        super.enterForkstmt(ctx);
    }

    @Override
    public void exitForkstmt(ForkstmtContext ctx)
    {
        if(fork)
        {
            update_EmptyLine_Prediction_Fork(-1);
        }
        else
        {
            generatedCode.append("\n");
        }

        generateTabs();
        generatedCode.append("Register.notifyTermination(Thread.currentThread(), this, getChildrenAlive());\n");

        tabDepth--;

        generateTabs();
        generatedCode.append("}\n");

        tabDepth--;

        generateTabs();
        generatedCode.append("};\n");

        super.exitForkstmt(ctx);
    }

    @Override
    public void enterCondition(ConditionContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.CONDITION));

        update_EmptyLine_Prediction_Fork(vertices.get(vertices.size() - 1).getId());

        generateTabs();
        generatedCode.append("if(" + ctx.expressionB1().getText() + ")\n");
        generateTabs();
        generatedCode.append("{\n");

        tabDepth++;

        ifStack.add(0);

        super.enterCondition(ctx);
    }

    @Override
    public void exitCondition(ConditionContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.CEND));

        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Register.setPrediction(Thread.currentThread(), this, " + vertices.get(vertices.size() - 1).getId() + ");\n");

        ifStack.remove(ifStack.size() - 1);

        super.exitCondition(ctx);
    }

    @Override
    public void enterIfstmt(IfstmtContext ctx)
    {
        if(ifStack.get(ifStack.size() - 1) == 1)
        {
            generateTabs();
            generatedCode.append("else\n");
            generateTabs();
            generatedCode.append("{\n");

            tabDepth++;
        }

        emptyLine = false;
        updatePrediction = true;

        super.enterIfstmt(ctx);
    }

    @Override
    public void exitIfstmt(IfstmtContext ctx)
    {
        emptyLine = true;
        updatePrediction = false;

        tabDepth--;

        generateTabs();
        generatedCode.append("}\n");

        ifStack.set(ifStack.size() - 1, ifStack.get(ifStack.size() - 1) + 1);

        super.exitIfstmt(ctx);
    }

    @Override
    public void enterLoop(LoopContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.WHILE));

        update_EmptyLine_Prediction_Fork(vertices.get(vertices.size() - 1).getId());

        generateTabs();
        generatedCode.append("while(" + ctx.expressionB1().getText() + ")\n");
        generateTabs();
        generatedCode.append("{\n");

        tabDepth++;

        emptyLine = false;

        super.enterLoop(ctx);
    }

    @Override
    public void exitLoop(LoopContext ctx)
    {
        vertices.add(new ControlFlowGraphNode(Type.WEND));

        tabDepth--;

        generateTabs();
        generatedCode.append("}\n");
        generatedCode.append("\n");
        generateTabs();
        generatedCode.append("Register.setPrediction(Thread.currentThread(), this, " + vertices.get(vertices.size() - 1).getId() + ");\n");

        emptyLine = true;

        super.exitLoop(ctx);
    }

    private void generateTabs()
    {
        for(int i = 0; i < tabDepth; i++)
        {
            generatedCode.append(tab);
        }
    }

    public String getGeneratedCode()
    {
        return generatedCode.toString();
    }
}