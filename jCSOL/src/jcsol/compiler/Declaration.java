package jcsol.compiler;

public class Declaration
{
    private String name  = null;
    private String type  = null;
    private int    layer = -1;

    public Declaration(String name, String type, int layer)
    {
        this.name = name;
        this.type = type;
        this.layer = layer;
    }

    public String getName()
    {
        return name;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public int getLayer()
    {
        return layer;
    }

    @Override
    public String toString()
    {
        return "name=" + name + ";type=" + type;
    }
}