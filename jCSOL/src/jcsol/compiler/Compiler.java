package jcsol.compiler;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.BitSet;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import jcsol.compiler.antlr4.CsolLexer;
import jcsol.compiler.antlr4.CsolParser;
import jcsol.compiler.antlr4.CsolParser.CsolProgramContext;
import jcsol.data.ControlFlowGraphNode;
import jcsol.util.Util;

public class Compiler
{
    private static final String            inputFilenameCSOL  = "p10_edited.csol";
    private static final String            outputFilenameJAVA = "src/jcsol/runtime/generated/GeneratedCode.java";

    private static final String            classPath          = "src;lib/gson-2.8.2.jar";
    private static final String            binaryPath         = "_bin";
    private static final String            sourceFile         = "src/jcsol/runtime/launch/Launcher.java";

    private static final ArrayList<String> tokens             = new ArrayList<String>();

    public static void main(String[] args)
    {
        Util.charsetUTF8();

        String content = Util.readFile(inputFilenameCSOL, Charset.defaultCharset());

        System.out.print("Analyzing " + inputFilenameCSOL + "\n\n");

        ANTLRErrorListener antlrErrorListener = new ANTLRErrorListener()
        {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            {
                System.err.print("Syntax error: " + msg + " (" + line + ":" + charPositionInLine + ")\n");

                System.exit(0);
            }

            @Override
            public void reportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, ATNConfigSet configs)
            {
                System.out.print("### reportContextSensitivity    ### Ambiguity successfully resolved\n");

                for(int i = startIndex; i <= stopIndex; i++)
                {
                    System.out.print(tokens.get(i));
                }

                System.out.print("\" (" + recognizer.getCurrentToken().getLine() + ":" + recognizer.getCurrentToken().getCharPositionInLine() + ")\n");
            }

            @Override
            public void reportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, ATNConfigSet configs)
            {
                System.out.print("### reportAttemptingFullContext ### Needs to expand the lookahead to handle ambiguity\n");

                for(int i = startIndex; i <= stopIndex; i++)
                {
                    System.out.print(tokens.get(i));
                }

                System.out.print("\" (" + recognizer.getCurrentToken().getLine() + ":" + recognizer.getCurrentToken().getCharPositionInLine() + ")\n");
            }

            @Override
            public void reportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, boolean exact, BitSet ambigAlts, ATNConfigSet configs)
            {
                System.err.print("Ambiguity found at \"");

                for(int i = startIndex; i <= stopIndex; i++)
                {
                    System.err.print(tokens.get(i));
                }

                System.err.print("\" (" + recognizer.getCurrentToken().getLine() + ":" + recognizer.getCurrentToken().getCharPositionInLine() + ")\n");

                System.exit(0);
            }
        };

        CsolLexer lexer = new CsolLexer(CharStreams.fromString(content));

        lexer.addErrorListener(antlrErrorListener);

        System.out.print("Lexical Analysis (Tokenization)...\n");

        int counter = 1;

        for(Token token = lexer.nextToken(); token.getType() != Token.EOF; token = lexer.nextToken())
        {
            tokens.add(token.getText());

            System.out.print(counter++ + " " + token.getStartIndex() + ":" + token.getStopIndex() + " \"" + token.getText() + "\" " + token.getType() + " " + lexer.getRuleNames()[token.getType() - 1] + " " + token.getLine() + ":" + token.getCharPositionInLine() + "\n");
        }

        lexer.reset();

        System.out.print("\nParsing...\n");

        CommonTokenStream tokens = new CommonTokenStream(lexer);

        CsolParser parser = new CsolParser(tokens);

        parser.getInterpreter().setPredictionMode(PredictionMode.LL_EXACT_AMBIG_DETECTION);
        parser.addErrorListener(antlrErrorListener);

        CsolProgramContext csolProgramContext = parser.csolProgram();

        String outputFilename = inputFilenameCSOL.split("\\.")[0];

        //Generate CFG
        System.out.print("\nParseTreeWalk to CFG...\n");

        //TODO
        //ParserListenerCFG parserListenerCFG = new ParserListenerCFG();
        ParserListenerCFG_deprecated parserListenerCFG = new ParserListenerCFG_deprecated();
        ParseTreeWalker.DEFAULT.walk(parserListenerCFG, csolProgramContext);

        Util.writeFile(outputFilename + ".json", parserListenerCFG.getControlFlowGraph().toString(), Charset.defaultCharset());

        System.out.print("\nWritten to " + outputFilename + ".json\n");

        //Generate dot file and render it to png
        Util.graphDotExport(outputFilename, parserListenerCFG.getControlFlowGraph(), false, false);

        System.out.print("Written to " + outputFilename + ".dot\n\n");

        System.out.print("Rendering dot to png...\n");

        //dot -Tpng outputFilename.dot -o outputFilename.png
        ProcessBuilder builder = new ProcessBuilder("dot", "-Tpng", outputFilename + ".dot", "-o", outputFilename + ".png");
        builder.redirectInput(ProcessBuilder.Redirect.INHERIT);
        builder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        builder.redirectError(ProcessBuilder.Redirect.INHERIT);

        try
        {
            builder.start().waitFor();

            System.out.print("Written to " + outputFilename + ".png\n");
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();

            System.exit(0);
        }
        catch(IOException e)
        {
            e.printStackTrace();

            System.out.print("\nPlease check whether you have installed a dot renderer like GraphViz (http://www.graphviz.org/) and if the binaries are available via your PATH variable.\n");
            System.out.print("The generated dot file was not rendered to png.\n");
        }

        //Generate Java Code
        System.out.print("\nParseTreeWalk to Java Code...\n");

        ParserListenerJAVA parserListenerJAVA = new ParserListenerJAVA(parserListenerCFG.getControlFlowGraph());
        ParseTreeWalker.DEFAULT.walk(parserListenerJAVA, csolProgramContext);

        Util.writeFile(outputFilenameJAVA, parserListenerJAVA.getGeneratedCode(), Charset.defaultCharset());

        System.out.print("Written to " + outputFilenameJAVA + "\n");

        System.out.print("\nPrediction\n");

        for(ControlFlowGraphNode vertex : parserListenerCFG.getControlFlowGraph().getVertices())
        {
            if(vertex.getPrediction() != null)
            {
                System.out.print("Node " + vertex.getId() + " " + vertex.getPrediction().getPrintablePrediction() + "\n");
            }
        }

        System.out.print("\n");

        //Compile Java Source Code
        System.out.print("Compiling " + inputFilenameCSOL + " from generated Java source code to Java byte code\n\n");

        //        File bin = new File(binaryPath);
        //
        //        Util.deleteDirectory(bin);
        //        Util.createDirectory(bin);
        //
        //        com.sun.tools.javac.Main.compile(new String[] { "-classpath", classPath, "-d", binaryPath, sourceFile });

        JavaCompiler javac = ToolProvider.getSystemJavaCompiler();

        if(javac != null)
        {
            File bin = new File(binaryPath);

            Util.deleteDirectory(bin);
            Util.createDirectory(bin);

            //javac -classpath src;lib/gson-2.8.2.jar;lib/antlr-4.7-complete.jar -d _bin_ src/jcsol/compiler/Compiler.java
            //javac -classpath src;lib/gson-2.8.2.jar -d _bin src/jcsol/runtime/launch/Launcher.java
            if(javac.run(System.in, System.out, System.err, "-classpath", classPath, "-d", binaryPath, sourceFile) == 0)
            {
                System.out.print("Compilation of " + inputFilenameCSOL + " successful\n\n");

                //Run Java Byte Code
                System.out.print("Executing " + inputFilenameCSOL + "...\n\n");

                //java -Dfile.encoding=UTF-8 -classpath binaryPath;lib/gson-2.8.2.jar jcsol/runtime/launch/Launcher outputFilename.json
                builder = new ProcessBuilder("java", "-Dfile.encoding=UTF-8", "-classpath", binaryPath + ";lib/gson-2.8.2.jar", "jcsol/runtime/launch/Launcher", outputFilename + ".json");
                builder.redirectInput(ProcessBuilder.Redirect.INHERIT);
                builder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
                builder.redirectError(ProcessBuilder.Redirect.INHERIT);

                try
                {
                    builder.start().waitFor();
                }
                catch(InterruptedException | IOException e)
                {
                    e.printStackTrace();

                    System.exit(0);
                }
            }
        }
        else
        {
            System.err.print("Java compiler not found! (tools.jar missing in classpath)\n");
            System.err.print("1. Download and install latest JDK\n");
            System.err.print("2. Eclipse -> Window -> Preferences -> Java -> Installed JREs -> Edit -> Add External JARs\n");
        }
    }
}