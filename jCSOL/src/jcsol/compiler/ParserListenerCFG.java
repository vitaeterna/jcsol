package jcsol.compiler;

import java.util.ArrayList;

import jcsol.compiler.antlr4.CsolBaseListener;
import jcsol.compiler.antlr4.CsolParser.AssignmentContext;
import jcsol.compiler.antlr4.CsolParser.ConditionContext;
import jcsol.compiler.antlr4.CsolParser.CsolProgramContext;
import jcsol.compiler.antlr4.CsolParser.DeclarationContext;
import jcsol.compiler.antlr4.CsolParser.ForkContext;
import jcsol.compiler.antlr4.CsolParser.ForkstmtContext;
import jcsol.compiler.antlr4.CsolParser.IfstmtContext;
import jcsol.compiler.antlr4.CsolParser.LoopContext;
import jcsol.compiler.antlr4.CsolParser.MethodCallContext;
import jcsol.compiler.antlr4.CsolParser.MultipleDecContext;
import jcsol.compiler.antlr4.CsolParser.PauseContext;
import jcsol.compiler.antlr4.CsolParser.SkipContext;
import jcsol.data.ControlFlowGraph;
import jcsol.data.ControlFlowGraphEdge;
import jcsol.data.ControlFlowGraphNode;
import jcsol.data.ControlFlowGraphNode.Type;
import jcsol.data.Prediction;

public class ParserListenerCFG extends CsolBaseListener
{
    private static final boolean                       CREATE_EDGE     = true;
    private static final boolean                       PREDICTION_NODE = true;

    private ArrayList<ArrayList<ControlFlowGraphNode>> conditions      = null;
    private ArrayList<ArrayList<ControlFlowGraphNode>> forks           = null;
    private ArrayList<ControlFlowGraphNode>            whiles          = null;

    private ControlFlowGraph                           cfg             = null;
    private ArrayList<ControlFlowGraphNode>            vertices        = null;
    private ArrayList<ControlFlowGraphEdge>            edges           = null;

    private ArrayList<ControlFlowGraphEdge>            addWhileEdges   = null;
    private ArrayList<ControlFlowGraphEdge>            delWhileEdges   = null;

    private ControlFlowGraphNode                       lastVertex      = null;
    private ControlFlowGraphNode                       currentVertex   = null;

    private boolean                                    conditional     = false;
    private boolean                                    fork            = false;
    private boolean                                    loop            = false;

    private int                                        currentGroup    = 0;

    public ParserListenerCFG()
    {
        conditions = new ArrayList<ArrayList<ControlFlowGraphNode>>();
        forks = new ArrayList<ArrayList<ControlFlowGraphNode>>();
        whiles = new ArrayList<ControlFlowGraphNode>();

        vertices = new ArrayList<ControlFlowGraphNode>();
        edges = new ArrayList<ControlFlowGraphEdge>();
        addWhileEdges = new ArrayList<ControlFlowGraphEdge>();
        delWhileEdges = new ArrayList<ControlFlowGraphEdge>();

        cfg = new ControlFlowGraph(vertices, edges);
        cfg.setWhileEdges(addWhileEdges);
        cfg.setInvisibleWhileEdges(delWhileEdges);

        ControlFlowGraphNode.resetIdCounter();
    }

    private void createAndAddVertex(Type type, String value, boolean createEdge, boolean predictionNode)
    {
        if(conditional || fork || loop)
        {
            currentGroup = ControlFlowGraphNode.getNextId();
        }
        else if(type.equals(Type.CEND))
        {
            currentGroup = conditions.get(conditions.size() - 1).get(0).getGroup();
        }
        else if(type.equals(Type.JOIN))
        {
            currentGroup = forks.get(forks.size() - 1).get(0).getGroup();
        }
        else if(type.equals(Type.WEND))
        {
            currentGroup = whiles.get(whiles.size() - 1).getGroup();
        }

        lastVertex = currentVertex;

        if(type.equals(Type.WEND) && whiles.size() == 1)
        {
            currentVertex = new ControlFlowGraphNode(type, value, false, currentGroup);
        }
        else
        {
            currentVertex = new ControlFlowGraphNode(type, value, !whiles.isEmpty(), currentGroup);
        }

        if(predictionNode)
        {
            currentVertex.setPrediction(new Prediction());
        }

        vertices.add(currentVertex);

        if(createEdge)
        {
            if(conditional)
            {
                if(conditions.get(conditions.size() - 1).size() == 1)
                {
                    edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "true"));
                }
                else if(conditions.get(conditions.size() - 1).size() == 2)
                {
                    edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "false"));
                }

                currentVertex.setPrediction(new Prediction());

                conditional = false;
            }
            else if(fork)
            {
                if(forks.get(forks.size() - 1).size() == 1)
                {
                    edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(0).getId(), currentVertex.getId(), "1"));
                }
                else if(forks.get(forks.size() - 1).size() == 2)
                {
                    edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(0).getId(), currentVertex.getId(), "2"));
                }

                currentVertex.setPrediction(new Prediction());

                fork = false;
            }
            else if(loop)
            {
                edges.add(new ControlFlowGraphEdge(lastVertex.getId(), currentVertex.getId(), "true"));

                loop = false;
            }
            else
            {
                edges.add(new ControlFlowGraphEdge(lastVertex.getId(), currentVertex.getId(), null));
            }
        }
    }

    @Override
    public void enterCsolProgram(CsolProgramContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.START, "start", !CREATE_EDGE, PREDICTION_NODE);

        super.enterCsolProgram(ctx);
    }

    @Override
    public void exitCsolProgram(CsolProgramContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.STOP, "stop", CREATE_EDGE, !PREDICTION_NODE);

        edges.addAll(addWhileEdges);
        edges.removeAll(delWhileEdges);

        cfg.analyze();

        super.exitCsolProgram(ctx);
    }

    @Override
    public void enterMethodCall(MethodCallContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.CALL, ctx.getText(), CREATE_EDGE, !PREDICTION_NODE);

        currentVertex.setIdentifier(ctx.IDENTIFIER(0).getText());
        currentVertex.setMethod(ctx.IDENTIFIER(1).getText());

        super.enterMethodCall(ctx);
    }

    @Override
    public void enterDeclaration(DeclarationContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.DECLARATION, null, CREATE_EDGE, !PREDICTION_NODE);

        currentVertex.addIdentifier(ctx.IDENTIFIER(0).getText());

        super.enterDeclaration(ctx);
    }

    @Override
    public void enterMultipleDec(MultipleDecContext ctx)
    {
        if(ctx.IDENTIFIER() != null)
        {
            currentVertex.addIdentifier(ctx.IDENTIFIER().getText());
        }

        super.enterMultipleDec(ctx);
    }

    @Override
    public void exitDeclaration(DeclarationContext ctx)
    {
        currentVertex.setSCClass(ctx.IDENTIFIER(1).getText());

        String value = "";

        for(String identifier : currentVertex.getIdentifiers())
        {
            value += identifier + ", ";
        }

        value = value.substring(0, value.length() - 2);
        value += " " + ctx.ASSIGN().getText() + " " + ctx.NEW().getText() + " " + ctx.IDENTIFIER(1).getText();

        currentVertex.setValue(value);

        super.exitDeclaration(ctx);
    }

    @Override
    public void enterSkip(SkipContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.SKIP, ctx.getText(), CREATE_EDGE, !PREDICTION_NODE);

        super.enterSkip(ctx);
    }

    @Override
    public void enterPause(PauseContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.PAUSE, ctx.getText(), CREATE_EDGE, PREDICTION_NODE);

        super.enterPause(ctx);
    }

    @Override
    public void enterAssignment(AssignmentContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.ASSIGN, ctx.type().getText() + " " + ctx.IDENTIFIER().getText() + " " + ctx.ASSIGN().getText(), CREATE_EDGE, !PREDICTION_NODE);

        super.enterAssignment(ctx);
    }

    @Override
    public void enterFork(ForkContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.FORK, "fork", CREATE_EDGE, !PREDICTION_NODE);

        forks.add(new ArrayList<ControlFlowGraphNode>());
        forks.get(forks.size() - 1).add(currentVertex);

        super.enterFork(ctx);
    }

    @Override
    public void exitFork(ForkContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.JOIN, "join", !CREATE_EDGE, PREDICTION_NODE);

        currentVertex.setSourceId(forks.get(forks.size() - 1).get(0).getId());

        if(forks.get(forks.size() - 1).get(1).getId() == forks.get(forks.size() - 1).get(0).getId())
        {
            edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(0).getId(), currentVertex.getId(), "1"));
        }
        else
        {
            edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(1).getId(), currentVertex.getId(), null));

            forks.get(forks.size() - 1).get(1).setBranchId(1);
        }

        if(forks.get(forks.size() - 1).get(2).getId() == forks.get(forks.size() - 1).get(0).getId() || forks.get(forks.size() - 1).get(2).getId() == forks.get(forks.size() - 1).get(1).getId())
        {
            edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(0).getId(), currentVertex.getId(), "2"));
        }
        else
        {
            edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(2).getId(), currentVertex.getId(), null));

            forks.get(forks.size() - 1).get(2).setBranchId(2);
        }

        forks.remove(forks.size() - 1);

        super.exitFork(ctx);
    }

    @Override
    public void enterForkstmt(ForkstmtContext ctx)
    {
        fork = true;

        super.enterForkstmt(ctx);
    }

    @Override
    public void exitForkstmt(ForkstmtContext ctx)
    {
        fork = false;

        forks.get(forks.size() - 1).add(currentVertex);

        super.exitForkstmt(ctx);
    }

    @Override
    public void enterCondition(ConditionContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.CONDITION, ctx.expressionB1().getText(), CREATE_EDGE, !PREDICTION_NODE);

        conditions.add(new ArrayList<ControlFlowGraphNode>());
        conditions.get(conditions.size() - 1).add(currentVertex);

        super.enterCondition(ctx);
    }

    @Override
    public void exitCondition(ConditionContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.CEND, "end", !CREATE_EDGE, PREDICTION_NODE);

        currentVertex.setSourceId(conditions.get(conditions.size() - 1).get(0).getId());

        if(conditions.get(conditions.size() - 1).size() == 2)
        {
            edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "false"));

            if(conditions.get(conditions.size() - 1).get(1).getId() == conditions.get(conditions.size() - 1).get(0).getId())
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "true"));
            }
            else
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(1).getId(), currentVertex.getId(), null));

                conditions.get(conditions.size() - 1).get(1).setBranchId(1);
            }
        }
        else if(conditions.get(conditions.size() - 1).size() == 3)
        {
            if(conditions.get(conditions.size() - 1).get(1).getId() == conditions.get(conditions.size() - 1).get(0).getId())
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "true"));
            }
            else
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(1).getId(), currentVertex.getId(), null));

                conditions.get(conditions.size() - 1).get(1).setBranchId(1);
            }

            if(conditions.get(conditions.size() - 1).get(2).getId() == conditions.get(conditions.size() - 1).get(0).getId() || conditions.get(conditions.size() - 1).get(2).getId() == conditions.get(conditions.size() - 1).get(1).getId())
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "false"));
            }
            else
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(2).getId(), currentVertex.getId(), null));

                conditions.get(conditions.size() - 1).get(2).setBranchId(2);
            }
        }

        conditions.remove(conditions.size() - 1);

        super.exitCondition(ctx);
    }

    @Override
    public void enterIfstmt(IfstmtContext ctx)
    {
        conditional = true;

        super.enterIfstmt(ctx);
    }

    @Override
    public void exitIfstmt(IfstmtContext ctx)
    {
        conditional = false;

        conditions.get(conditions.size() - 1).add(currentVertex);

        super.exitIfstmt(ctx);
    }

    @Override
    public void enterLoop(LoopContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.WHILE, ctx.expressionB1().getText(), CREATE_EDGE, !PREDICTION_NODE);

        whiles.add(currentVertex);

        loop = true;

        super.enterLoop(ctx);
    }

    @Override
    public void exitLoop(LoopContext ctx)
    {
        loop = false;

        createAndAddVertex(ControlFlowGraphNode.Type.WEND, "end", CREATE_EDGE, PREDICTION_NODE);

        currentVertex.setSourceId(whiles.get(whiles.size() - 1).getId());

        delWhileEdges.add(edges.get(edges.size() - 1));

        addWhileEdges.add(new ControlFlowGraphEdge(whiles.get(whiles.size() - 1).getId(), currentVertex.getId(), "false"));

        if(lastVertex.getId() == whiles.get(whiles.size() - 1).getId())
        {
            addWhileEdges.add(new ControlFlowGraphEdge(lastVertex.getId(), whiles.get(whiles.size() - 1).getId(), "true;jump"));
        }
        else
        {
            addWhileEdges.add(new ControlFlowGraphEdge(lastVertex.getId(), whiles.get(whiles.size() - 1).getId(), "jump"));
        }

        whiles.remove(whiles.size() - 1);

        super.exitLoop(ctx);
    }

    public ControlFlowGraph getControlFlowGraph()
    {
        return cfg;
    }
}