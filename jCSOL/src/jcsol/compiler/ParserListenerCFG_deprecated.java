package jcsol.compiler;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import jcsol.compiler.antlr4.CsolBaseListener;
import jcsol.compiler.antlr4.CsolParser.AssignmentContext;
import jcsol.compiler.antlr4.CsolParser.ConditionContext;
import jcsol.compiler.antlr4.CsolParser.CsolProgramContext;
import jcsol.compiler.antlr4.CsolParser.DeclarationContext;
import jcsol.compiler.antlr4.CsolParser.ForkContext;
import jcsol.compiler.antlr4.CsolParser.ForkstmtContext;
import jcsol.compiler.antlr4.CsolParser.IfstmtContext;
import jcsol.compiler.antlr4.CsolParser.LoopContext;
import jcsol.compiler.antlr4.CsolParser.MethodCallContext;
import jcsol.compiler.antlr4.CsolParser.MultipleDecContext;
import jcsol.compiler.antlr4.CsolParser.PauseContext;
import jcsol.compiler.antlr4.CsolParser.SkipContext;
import jcsol.data.ControlFlowGraph;
import jcsol.data.ControlFlowGraphEdge;
import jcsol.data.ControlFlowGraphNode;
import jcsol.data.ControlFlowGraphNode.Type;
import jcsol.data.Prediction;
import jcsol.runtime.schedule.StatefulPrecedingMethods;

public class ParserListenerCFG_deprecated extends CsolBaseListener
{
    private static final boolean                       CREATE_EDGE        = true;
    private static final boolean                       PREDICTION_NODE    = true;

    private ArrayList<ArrayList<ControlFlowGraphNode>> conditions         = null;
    private ArrayList<ArrayList<ControlFlowGraphNode>> forks              = null;
    private ArrayList<ControlFlowGraphNode>            whiles             = null;

    private ControlFlowGraph                           cfg                = null;
    private ArrayList<ControlFlowGraphNode>            vertices           = null;
    private ArrayList<ControlFlowGraphEdge>            edges              = null;

    private ArrayList<ControlFlowGraphEdge>            addWhileEdges      = null;
    private ArrayList<ControlFlowGraphEdge>            delWhileEdges      = null;

    private ArrayList<ControlFlowGraphNode>            nodesInWhile       = null;
    private boolean                                    stillInWhile       = false;

    private ControlFlowGraphNode                       lastVertex         = null;
    private ControlFlowGraphNode                       currentVertex      = null;

    private boolean                                    conditional        = false;
    private boolean                                    fork               = false;
    private boolean                                    loop               = false;

    private int                                        currentGroup       = 0;

    private ArrayList<ControlFlowGraphNode>            invisVertices      = null;
    private ArrayList<ControlFlowGraphEdge>            invisEdges         = null;
    private ArrayList<ControlFlowGraphNode>            sameRankedVertices = null;
    private ArrayList<ControlFlowGraphNode>            tempRankedVertices = null;
    private ArrayList<ControlFlowGraphNode>            visitedVertices    = null;
    private int[]                                      joinCounters       = null;
    private ArrayList<ArrayList<ControlFlowGraphNode>> forkStack          = null;
    private ArrayList<ControlFlowGraphNode>            tempFork           = null;
    private ArrayList<ArrayList<Integer>>              sameRanks          = null;

    public ParserListenerCFG_deprecated()
    {
        conditions = new ArrayList<ArrayList<ControlFlowGraphNode>>();
        forks = new ArrayList<ArrayList<ControlFlowGraphNode>>();
        whiles = new ArrayList<ControlFlowGraphNode>();

        vertices = new ArrayList<ControlFlowGraphNode>();
        edges = new ArrayList<ControlFlowGraphEdge>();
        cfg = new ControlFlowGraph(vertices, edges);

        addWhileEdges = new ArrayList<ControlFlowGraphEdge>();
        delWhileEdges = new ArrayList<ControlFlowGraphEdge>();

        nodesInWhile = new ArrayList<ControlFlowGraphNode>();

        ControlFlowGraphNode.resetIdCounter();
    }

    private void createAndAddVertex(Type type, String value, boolean createEdge, boolean predictionNode)
    {
        if(conditional || fork || loop)
        {
            currentGroup = ControlFlowGraphNode.getNextId();
        }
        else if(type.equals(Type.CEND))
        {
            currentGroup = conditions.get(conditions.size() - 1).get(0).getGroup();
        }
        else if(type.equals(Type.JOIN))
        {
            currentGroup = forks.get(forks.size() - 1).get(0).getGroup();
        }
        else if(type.equals(Type.WEND))
        {
            currentGroup = whiles.get(whiles.size() - 1).getGroup();
        }

        lastVertex = currentVertex;

        if(type.equals(Type.WEND) && whiles.size() == 1)
        {
            currentVertex = new ControlFlowGraphNode(type, value, false, currentGroup);
        }
        else
        {
            currentVertex = new ControlFlowGraphNode(type, value, !whiles.isEmpty(), currentGroup);
        }

        if(predictionNode)
        {
            currentVertex.setPrediction(new Prediction());
        }

        vertices.add(currentVertex);

        if(currentVertex.inWhile() && !stillInWhile)
        {
            stillInWhile = true;

            nodesInWhile.add(currentVertex);
        }
        else if(currentVertex.inWhile() && stillInWhile)
        {
            nodesInWhile.add(currentVertex);
        }
        else if(!currentVertex.inWhile() && stillInWhile)
        {
            nodesInWhile.add(0, whiles.get(whiles.size() - 1));

            edges.add(new ControlFlowGraphEdge(lastVertex.getId(), whiles.get(whiles.size() - 1).getId(), "tempJump"));

            for(ControlFlowGraphNode vertex : nodesInWhile)
            {
                if(vertex.getType().equals(Type.CALL))
                {
                    refreshInWhile(vertex);

                    propagateSharedAccessWithinWhile(vertex);
                }
            }

            edges.remove(edges.size() - 1);

            nodesInWhile.clear();

            stillInWhile = false;
        }

        if(createEdge)
        {
            if(conditional)
            {
                if(conditions.get(conditions.size() - 1).size() == 1)
                {
                    edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "true"));
                }
                else if(conditions.get(conditions.size() - 1).size() == 2)
                {
                    edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "false"));
                }

                currentVertex.setPrediction(new Prediction());

                conditional = false;
            }
            else if(fork)
            {
                if(forks.get(forks.size() - 1).size() == 1)
                {
                    edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(0).getId(), currentVertex.getId(), "1"));
                }
                else if(forks.get(forks.size() - 1).size() == 2)
                {
                    edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(0).getId(), currentVertex.getId(), "2"));
                }

                currentVertex.setPrediction(new Prediction());

                fork = false;
            }
            else if(loop)
            {
                edges.add(new ControlFlowGraphEdge(lastVertex.getId(), currentVertex.getId(), "true"));

                loop = false;
            }
            else
            {
                edges.add(new ControlFlowGraphEdge(lastVertex.getId(), currentVertex.getId(), null));
            }
        }
    }

    private void propagateSharedAccess()
    {
        System.out.print("\npropagate " + currentVertex.getIdentifier() + "." + currentVertex.getMethod() + "\n");

        ArrayList<ControlFlowGraphNode> todo = new ArrayList<ControlFlowGraphNode>();
        todo.add(currentVertex);

        ControlFlowGraphNode currentNode = null;
        Prediction currentPrediction = null;
        ArrayList<ControlFlowGraphNode> predecessors = null;
        Integer value = null;
        HashMap<Integer, Integer> forkStack = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> condStack = new HashMap<Integer, Integer>();
        boolean addParents = false;
        boolean inWhile = currentVertex.inWhile();
        boolean stillInFirstWhile = currentVertex.inWhile();

        for(int index = 0; index < todo.size(); index++)
        {
            currentNode = todo.get(index);

            if(stillInFirstWhile)
            {
                stillInFirstWhile = currentNode.inWhile();
            }

            System.out.print("node " + currentNode.getId() + " " + stillInFirstWhile + " ");

            if(currentNode.getType().equals(Type.JOIN))
            {
                forkStack.put(currentNode.getSourceId(), 2);
            }
            else if(currentNode.getType().equals(Type.CEND))
            {
                condStack.put(currentNode.getSourceId(), 2);
            }
            else if(currentNode.getType().equals(Type.FORK))
            {
                value = forkStack.get(currentNode.getId());

                if(value != null)
                {
                    forkStack.put(currentNode.getId(), value - 1);

                    if(forkStack.get(currentNode.getId()).equals(0))
                    {
                        forkStack.remove(currentNode.getId());
                    }
                }
            }
            else if(currentNode.getType().equals(Type.CONDITION))
            {
                value = condStack.get(currentNode.getId());

                if(value != null)
                {
                    condStack.put(currentNode.getId(), value - 1);

                    if(condStack.get(currentNode.getId()).equals(0))
                    {
                        condStack.remove(currentNode.getId());
                    }
                }
            }

            if((forkStack.isEmpty() || forkStack.size() == 1 && currentNode.getType().equals(Type.JOIN)) && !stillInFirstWhile)
            {
                currentPrediction = currentNode.getPrediction();

                if(currentPrediction != null)
                {
                    currentPrediction.incrementPrediction(currentVertex.getIdentifier(), currentVertex.getMethod(), inWhile);

                    System.out.print("updated, inWhile=" + inWhile + " ");
                }
            }

            addParents = !(currentNode.getType().equals(Type.PAUSE) && forkStack.isEmpty());
            addParents &= !(currentNode.getType().equals(Type.FORK) && forkStack.get(currentNode.getId()) != null);
            addParents &= !(currentNode.getType().equals(Type.CONDITION) && condStack.get(currentNode.getId()) != null);

            if(addParents)
            {
                predecessors = cfg.getPredecessors(currentNode.getId());

                System.out.print("-> " + predecessors.toString());

                todo.addAll(predecessors);
            }

            System.out.print("\n");
        }
    }

    private void refreshInWhile(ControlFlowGraphNode vertexToRefresh)
    {
        System.out.print("\nrefreshing inwhile of " + vertexToRefresh.getId() + "\n");

        ControlFlowGraphNode temp = vertexToRefresh;

        while(true)
        {
            if(temp.getType().equals(Type.PAUSE) || temp.getType().equals(Type.FORK))
            {
                System.out.print(temp.getType() + " -> false\n");

                vertexToRefresh.setInWhile(false);

                break;
            }
            else if(temp.getType().equals(Type.WHILE))
            {
                System.out.print(temp.getType() + " -> true\n");

                vertexToRefresh.setInWhile(true);

                break;
            }
            else
            {
                System.out.print(temp.getId() + "_" + temp.getType() + " -> ");

                if(temp.getSourceId() != 0)
                {
                    temp = vertices.get(temp.getSourceId());

                    System.out.print(temp.getId() + "_" + temp.getType() + " -> ");
                }

                for(ControlFlowGraphEdge edge : edges)
                {
                    if(edge.getId1() == temp.getId())
                    {
                        temp = vertices.get(edge.getId0());

                        System.out.print(temp.getId() + "_" + temp.getType() + "\n");

                        break;
                    }
                }
            }
        }
    }

    private void propagateSharedAccessWithinWhile(ControlFlowGraphNode vertexToPropagate)
    {
        System.out.print("\npropagate " + vertexToPropagate.getIdentifier() + "." + vertexToPropagate.getMethod() + " of " + vertexToPropagate.getId() + " inWhile\n");

        ArrayList<ControlFlowGraphNode> visited = new ArrayList<ControlFlowGraphNode>();
        ArrayList<ControlFlowGraphNode> todo = new ArrayList<ControlFlowGraphNode>();
        todo.add(vertexToPropagate);

        ControlFlowGraphNode currentNode = null;
        ControlFlowGraphNode sourceNode = null;
        ControlFlowGraphNode targetNode = null;
        Prediction currentPrediction = null;
        Integer value = null;
        HashMap<Integer, Integer> forkStack = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> condStack = new HashMap<Integer, Integer>();
        boolean addParents = false;

        for(int i = 0; i < todo.size(); i++)
        {
            currentNode = todo.get(i);

            if(!visited.contains(currentNode))
            {
                System.out.print("node " + currentNode.getId() + " ");

                if(currentNode.getType().equals(Type.JOIN))
                {
                    forkStack.put(currentNode.getSourceId(), 2);

                    visited.add(currentNode);
                }
                else if(currentNode.getType().equals(Type.CEND))
                {
                    condStack.put(currentNode.getSourceId(), 2);

                    visited.add(currentNode);
                }
                else if(currentNode.getType().equals(Type.FORK))
                {
                    value = forkStack.get(currentNode.getId());

                    if(value != null)
                    {
                        forkStack.put(currentNode.getId(), value - 1);

                        if(forkStack.get(currentNode.getId()).equals(0))
                        {
                            forkStack.remove(currentNode.getId());

                            visited.add(currentNode);
                        }
                    }
                }
                else if(currentNode.getType().equals(Type.CONDITION))
                {
                    value = condStack.get(currentNode.getId());

                    if(value != null)
                    {
                        condStack.put(currentNode.getId(), value - 1);

                        if(condStack.get(currentNode.getId()).equals(0))
                        {
                            condStack.remove(currentNode.getId());

                            visited.add(currentNode);
                        }
                    }
                }
                else
                {
                    visited.add(currentNode);
                }

                if(forkStack.isEmpty() || (forkStack.size() == 1 && currentNode.getType().equals(Type.JOIN)))
                {
                    currentPrediction = currentNode.getPrediction();

                    if(currentPrediction != null)
                    {
                        ControlFlowGraphNode temp = currentNode;
                        boolean inWhile = true;

                        System.out.print("\ncalculating inWhile:\n");

                        while(true)
                        {
                            if(temp.getType().equals(Type.PAUSE) || temp.getType().equals(Type.FORK))
                            {
                                System.out.print(temp.getType() + " -> false\n");

                                inWhile = false;

                                break;
                            }
                            else if(temp.getType().equals(Type.WHILE))
                            {
                                System.out.print(temp.getType() + " -> true\n");

                                inWhile = true;

                                break;
                            }
                            else
                            {
                                System.out.print(temp.getId() + "_" + temp.getType() + " -> ");

                                if(temp.getSourceId() != 0)
                                {
                                    temp = vertices.get(temp.getSourceId());

                                    System.out.print(temp.getId() + "_" + temp.getType() + " -> ");
                                }

                                for(ControlFlowGraphEdge edge : edges)
                                {
                                    if(edge.getId1() == temp.getId())
                                    {
                                        temp = vertices.get(edge.getId0());

                                        System.out.print(temp.getId() + "_" + temp.getType() + "\n");

                                        break;
                                    }
                                }
                            }
                        }

                        currentPrediction.incrementPrediction(vertexToPropagate.getIdentifier(), vertexToPropagate.getMethod(), inWhile);

                        System.out.print("updated, inWhile=" + inWhile + " ");
                    }
                }

                addParents = !(currentNode.getType().equals(Type.PAUSE) && forkStack.isEmpty());
                addParents &= !(currentNode.getType().equals(Type.FORK) && forkStack.get(currentNode.getId()) != null);
                addParents &= !(currentNode.getType().equals(Type.CONDITION) && condStack.get(currentNode.getId()) != null);

                if(addParents)
                {
                    for(ControlFlowGraphEdge edge : edges)
                    {
                        targetNode = vertices.get(edge.getId1());

                        if(currentNode == targetNode)
                        {
                            sourceNode = vertices.get(edge.getId0());

                            if(nodesInWhile.contains(sourceNode))
                            {
                                System.out.print("-> " + edge.getId0() + " ");

                                todo.add(sourceNode);
                            }
                        }
                    }
                }

                System.out.print("\n");
            }
        }
    }

    private void calculatePrecedenceEdges()
    {
        ArrayList<String> scClasses = new ArrayList<String>();

        for(ControlFlowGraphNode vertex0 : vertices)
        {
            if(vertex0.getType().equals(Type.DECLARATION))
            {
                if(!scClasses.contains(vertex0.getSCClass()))
                {
                    scClasses.add(vertex0.getSCClass());
                }

                for(ControlFlowGraphNode vertex1 : vertices)
                {
                    if(vertex1.getType().equals(Type.CALL))
                    {
                        if(vertex0.getIdentifiers().contains(vertex1.getIdentifier()))
                        {
                            vertex1.setSCClass(vertex0.getSCClass());
                        }
                    }
                }
            }
        }

        HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>> statefulPrecedences = new HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>();

        StatefulPrecedingMethods statefulPrecedingMethods = null;

        for(String scClass : scClasses)
        {
            statefulPrecedences.put(scClass, new HashMap<String, HashMap<String, ArrayList<String>>>());

            try
            {
                for(Method method : Class.forName("jcsol.runtime.sharedClasses." + scClass).getDeclaredMethods())
                {
                    if(method.isAnnotationPresent(StatefulPrecedingMethods.class))
                    {
                        statefulPrecedingMethods = method.getAnnotation(StatefulPrecedingMethods.class);

                        for(int index = 0; index < statefulPrecedingMethods.states().length; index++)
                        {
                            if(statefulPrecedences.get(scClass).get(statefulPrecedingMethods.states()[index]) == null)
                            {
                                statefulPrecedences.get(scClass).put(statefulPrecedingMethods.states()[index], new HashMap<String, ArrayList<String>>());
                            }

                            if(statefulPrecedences.get(scClass).get(statefulPrecedingMethods.states()[index]).get(method.getName()) != null)
                            {
                                System.err.print("Duplicate state found: \"" + statefulPrecedingMethods.states()[index] + "\"\n");

                                System.exit(0);
                            }

                            statefulPrecedences.get(scClass).get(statefulPrecedingMethods.states()[index]).put(method.getName(), new ArrayList<String>(Arrays.asList(statefulPrecedingMethods.methods()[index].split("\\."))));
                        }
                    }
                }
            }
            catch(ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }

        ArrayList<ControlFlowGraphEdge> precedenceEdges = new ArrayList<ControlFlowGraphEdge>();

        for(ControlFlowGraphNode vertex0 : vertices)
        {
            if(vertex0.getType().equals(Type.CALL))
            {
                for(ControlFlowGraphNode vertex1 : vertices)
                {
                    if(vertex1.getType().equals(Type.CALL))
                    {
                        if(vertex0.getSCClass().equals(vertex1.getSCClass()) && vertex0.getIdentifier().equals(vertex1.getIdentifier()))
                        {
                            for(Entry<String, HashMap<String, ArrayList<String>>> entry : statefulPrecedences.get(vertex0.getSCClass()).entrySet())
                            {
                                if(entry.getValue().get(vertex0.getMethod()).contains(vertex1.getMethod()))
                                {
                                    precedenceEdges.add(new ControlFlowGraphEdge(vertex1.getId(), vertex0.getId(), entry.getKey()));
                                }
                            }
                        }
                    }
                }
            }
        }

        cfg.setPrecedenceEdges(precedenceEdges);
    }

    private void calculateInvisibles()
    {
        invisVertices = new ArrayList<ControlFlowGraphNode>();
        invisEdges = new ArrayList<ControlFlowGraphEdge>();

        sameRankedVertices = new ArrayList<ControlFlowGraphNode>();
        tempRankedVertices = new ArrayList<ControlFlowGraphNode>();
        visitedVertices = new ArrayList<ControlFlowGraphNode>();
        joinCounters = new int[vertices.size()];
        forkStack = new ArrayList<ArrayList<ControlFlowGraphNode>>();
        sameRanks = new ArrayList<ArrayList<Integer>>();

        sameRankedVertices.add(vertices.get(0));

        while(!sameRankedVertices.isEmpty())
        {
            ArrayList<Integer> tempList = new ArrayList<Integer>();

            for(ControlFlowGraphNode vertex : sameRankedVertices)
            {
                tempList.add(vertex.getId());
            }

            sameRanks.add(tempList);

            tempRankedVertices = new ArrayList<ControlFlowGraphNode>();

            for(ControlFlowGraphNode vertex : sameRankedVertices)
            {
                if(vertex.getType() == null)
                {
                    continue;
                }

                visitedVertices.add(vertex);

                if(vertex.getType().equals(Type.WHILE) || vertex.getType().equals(Type.CONDITION) || vertex.getType().equals(Type.FORK))
                {
                    tempFork = new ArrayList<ControlFlowGraphNode>();
                    tempFork.add(vertex);
                    forkStack.add(tempFork);
                }

                for(ControlFlowGraphEdge edge : edges)
                {
                    if(edge.getId0() == vertex.getId() && !visitedVertices.contains(vertices.get(edge.getId1())) && !tempRankedVertices.contains(vertices.get(edge.getId1())))
                    {
                        if(vertices.get(edge.getId1()).getSourceId() != 0)
                        {
                            joinCounters[edge.getId1()]++;

                            if(joinCounters[edge.getId1()] == 2)
                            {
                                tempRankedVertices.add(vertices.get(edge.getId1()));
                            }
                        }
                        else
                        {
                            tempRankedVertices.add(vertices.get(edge.getId1()));
                        }
                    }
                }
            }

            for(ControlFlowGraphNode vertex : tempRankedVertices)
            {
                if(vertex.getType().equals(Type.WEND) || vertex.getType().equals(Type.CEND) || vertex.getType().equals(Type.JOIN))
                {
                    for(ArrayList<ControlFlowGraphNode> fork : forkStack)
                    {
                        if(fork.get(0).getId() == vertex.getSourceId())
                        {
                            invisEdges.add(new ControlFlowGraphEdge(fork.get(fork.size() - 1).getId(), vertex.getId(), null));

                            forkStack.remove(fork);

                            break;
                        }
                    }
                }
            }

            for(ArrayList<ControlFlowGraphNode> fork : forkStack)
            {
                ControlFlowGraphNode tempVertex = new ControlFlowGraphNode(null, fork.get(fork.size() - 1).getGroup());

                invisEdges.add(new ControlFlowGraphEdge(fork.get(fork.size() - 1).getId(), tempVertex.getId(), null));

                fork.add(tempVertex);

                invisVertices.add(tempVertex);

                tempRankedVertices.add(tempVertex);
            }

            sameRankedVertices = tempRankedVertices;
        }

        cfg.setFillerRanks(sameRanks);
        cfg.setInvisibleFillerVertices(invisVertices);
        cfg.setInvisibleFillerEdges(invisEdges);
    }

    private void calculateReducedRanks()
    {
        ArrayList<ArrayList<Integer>> reducedRanks = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> reducedRank = null;

        for(ArrayList<Integer> sameRank : sameRanks)
        {
            reducedRank = new ArrayList<Integer>();

            for(Integer integer : sameRank)
            {
                if(integer < invisVertices.get(0).getId())
                {
                    reducedRank.add(integer);
                }
            }

            reducedRanks.add(reducedRank);
        }

        cfg.setRanks(reducedRanks);
    }

    @Override
    public void enterCsolProgram(CsolProgramContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.START, "start", !CREATE_EDGE, PREDICTION_NODE);

        super.enterCsolProgram(ctx);
    }

    @Override
    public void exitCsolProgram(CsolProgramContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.STOP, "stop", CREATE_EDGE, !PREDICTION_NODE);

        cfg.setInvisibleWhileEdges(delWhileEdges);

        edges.addAll(addWhileEdges);

        calculatePrecedenceEdges();
        calculateInvisibles();
        calculateReducedRanks();

        edges.removeAll(delWhileEdges);

        super.exitCsolProgram(ctx);
    }

    @Override
    public void enterMethodCall(MethodCallContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.CALL, ctx.getText(), CREATE_EDGE, !PREDICTION_NODE);

        currentVertex.setIdentifier(ctx.IDENTIFIER(0).getText());
        currentVertex.setMethod(ctx.IDENTIFIER(1).getText());

        propagateSharedAccess();

        super.enterMethodCall(ctx);
    }

    @Override
    public void enterDeclaration(DeclarationContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.DECLARATION, null, CREATE_EDGE, !PREDICTION_NODE);

        currentVertex.addIdentifier(ctx.IDENTIFIER(0).getText());

        super.enterDeclaration(ctx);
    }

    @Override
    public void enterMultipleDec(MultipleDecContext ctx)
    {
        if(ctx.IDENTIFIER() != null)
        {
            currentVertex.addIdentifier(ctx.IDENTIFIER().getText());
        }

        super.enterMultipleDec(ctx);
    }

    @Override
    public void exitDeclaration(DeclarationContext ctx)
    {
        currentVertex.setSCClass(ctx.IDENTIFIER(1).getText());

        String value = "";

        for(String identifier : currentVertex.getIdentifiers())
        {
            value += identifier + ", ";
        }

        value = value.substring(0, value.length() - 2);
        value += " " + ctx.ASSIGN().getText() + " " + ctx.NEW().getText() + " " + ctx.IDENTIFIER(1).getText();

        currentVertex.setValue(value);

        super.exitDeclaration(ctx);
    }

    @Override
    public void enterSkip(SkipContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.SKIP, ctx.getText(), CREATE_EDGE, !PREDICTION_NODE);

        super.enterSkip(ctx);
    }

    @Override
    public void enterPause(PauseContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.PAUSE, ctx.getText(), CREATE_EDGE, PREDICTION_NODE);

        super.enterPause(ctx);
    }

    @Override
    public void enterAssignment(AssignmentContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.ASSIGN, ctx.type().getText() + " " + ctx.IDENTIFIER().getText() + " " + ctx.ASSIGN().getText(), CREATE_EDGE, !PREDICTION_NODE);

        super.enterAssignment(ctx);
    }

    @Override
    public void enterFork(ForkContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.FORK, "fork", CREATE_EDGE, !PREDICTION_NODE);

        forks.add(new ArrayList<ControlFlowGraphNode>());
        forks.get(forks.size() - 1).add(currentVertex);

        super.enterFork(ctx);
    }

    @Override
    public void exitFork(ForkContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.JOIN, "join", !CREATE_EDGE, PREDICTION_NODE);

        currentVertex.setSourceId(forks.get(forks.size() - 1).get(0).getId());

        if(forks.get(forks.size() - 1).get(1).getId() == forks.get(forks.size() - 1).get(0).getId())
        {
            edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(0).getId(), currentVertex.getId(), "1"));
        }
        else
        {
            edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(1).getId(), currentVertex.getId(), null));

            forks.get(forks.size() - 1).get(1).setBranchId(1);
        }

        if(forks.get(forks.size() - 1).get(2).getId() == forks.get(forks.size() - 1).get(0).getId() || forks.get(forks.size() - 1).get(2).getId() == forks.get(forks.size() - 1).get(1).getId())
        {
            edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(0).getId(), currentVertex.getId(), "2"));
        }
        else
        {
            edges.add(new ControlFlowGraphEdge(forks.get(forks.size() - 1).get(2).getId(), currentVertex.getId(), null));

            forks.get(forks.size() - 1).get(2).setBranchId(2);
        }

        forks.remove(forks.size() - 1);

        super.exitFork(ctx);
    }

    @Override
    public void enterForkstmt(ForkstmtContext ctx)
    {
        fork = true;

        super.enterForkstmt(ctx);
    }

    @Override
    public void exitForkstmt(ForkstmtContext ctx)
    {
        fork = false;

        forks.get(forks.size() - 1).add(currentVertex);

        super.exitForkstmt(ctx);
    }

    @Override
    public void enterCondition(ConditionContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.CONDITION, ctx.expressionB1().getText(), CREATE_EDGE, !PREDICTION_NODE);

        conditions.add(new ArrayList<ControlFlowGraphNode>());
        conditions.get(conditions.size() - 1).add(currentVertex);

        super.enterCondition(ctx);
    }

    @Override
    public void exitCondition(ConditionContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.CEND, "end", !CREATE_EDGE, PREDICTION_NODE);

        currentVertex.setSourceId(conditions.get(conditions.size() - 1).get(0).getId());

        if(conditions.get(conditions.size() - 1).size() == 2)
        {
            edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "false"));

            if(conditions.get(conditions.size() - 1).get(1).getId() == conditions.get(conditions.size() - 1).get(0).getId())
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "true"));
            }
            else
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(1).getId(), currentVertex.getId(), null));

                conditions.get(conditions.size() - 1).get(1).setBranchId(1);
            }
        }
        else if(conditions.get(conditions.size() - 1).size() == 3)
        {
            if(conditions.get(conditions.size() - 1).get(1).getId() == conditions.get(conditions.size() - 1).get(0).getId())
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "true"));
            }
            else
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(1).getId(), currentVertex.getId(), null));

                conditions.get(conditions.size() - 1).get(1).setBranchId(1);
            }

            if(conditions.get(conditions.size() - 1).get(2).getId() == conditions.get(conditions.size() - 1).get(0).getId() || conditions.get(conditions.size() - 1).get(2).getId() == conditions.get(conditions.size() - 1).get(1).getId())
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(0).getId(), currentVertex.getId(), "false"));
            }
            else
            {
                edges.add(new ControlFlowGraphEdge(conditions.get(conditions.size() - 1).get(2).getId(), currentVertex.getId(), null));

                conditions.get(conditions.size() - 1).get(2).setBranchId(2);
            }
        }

        conditions.remove(conditions.size() - 1);

        super.exitCondition(ctx);
    }

    @Override
    public void enterIfstmt(IfstmtContext ctx)
    {
        conditional = true;

        super.enterIfstmt(ctx);
    }

    @Override
    public void exitIfstmt(IfstmtContext ctx)
    {
        conditional = false;

        conditions.get(conditions.size() - 1).add(currentVertex);

        super.exitIfstmt(ctx);
    }

    @Override
    public void enterLoop(LoopContext ctx)
    {
        createAndAddVertex(ControlFlowGraphNode.Type.WHILE, ctx.expressionB1().getText(), CREATE_EDGE, !PREDICTION_NODE);

        whiles.add(currentVertex);

        loop = true;

        super.enterLoop(ctx);
    }

    @Override
    public void exitLoop(LoopContext ctx)
    {
        loop = false;

        createAndAddVertex(ControlFlowGraphNode.Type.WEND, "end", CREATE_EDGE, PREDICTION_NODE);

        currentVertex.setSourceId(whiles.get(whiles.size() - 1).getId());

        delWhileEdges.add(edges.get(edges.size() - 1));

        addWhileEdges.add(new ControlFlowGraphEdge(whiles.get(whiles.size() - 1).getId(), currentVertex.getId(), "false"));

        if(lastVertex.getId() == whiles.get(whiles.size() - 1).getId())
        {
            addWhileEdges.add(new ControlFlowGraphEdge(lastVertex.getId(), whiles.get(whiles.size() - 1).getId(), "true;jump"));
        }
        else
        {
            addWhileEdges.add(new ControlFlowGraphEdge(lastVertex.getId(), whiles.get(whiles.size() - 1).getId(), "jump"));
        }

        whiles.remove(whiles.size() - 1);

        super.exitLoop(ctx);
    }

    public ControlFlowGraph getControlFlowGraph()
    {
        return cfg;
    }
}