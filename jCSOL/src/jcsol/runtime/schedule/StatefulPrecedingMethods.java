package jcsol.runtime.schedule;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
public @interface StatefulPrecedingMethods
{
    public String[] states() default {};

    public String[] methods() default {};
}