package jcsol.runtime.schedule;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import jcsol.data.Prediction;

public abstract class ClockSynchronizedObject
{
    private String                      identifier        = null;
    private PolicyAutomaton             policyAutomaton   = null;
    private HashMap<Thread, Prediction> registeredThreads = null;

    public ClockSynchronizedObject()
    {
        policyAutomaton = getPolicyAutomaton();

        registeredThreads = new HashMap<Thread, Prediction>();
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public Prediction getEmptyPrediction()
    {
        Prediction emptyContext = new Prediction();

        emptyContext.getMap().put(identifier, new HashMap<String, Integer>());

        StatefulPrecedingMethods statefulPrecedingMethods = null;

        for(Method method : this.getClass().getDeclaredMethods())
        {
            if(method.isAnnotationPresent(StatefulPrecedingMethods.class))
            {
                statefulPrecedingMethods = method.getAnnotation(StatefulPrecedingMethods.class);

                if(statefulPrecedingMethods.states().length == 0 && statefulPrecedingMethods.methods().length == 0)
                {
                    continue;
                }
            }
            else
            {
                continue;
            }

            emptyContext.getMap().get(identifier).put(method.getName(), 0);
        }

        return emptyContext;
    }

    public void registerThreads(HashMap<Thread, Prediction> registerUpdate)
    {
        synchronized(this)
        {
            registeredThreads.putAll(registerUpdate);
        }
    }

    public void removeThread(Thread thread)
    {
        synchronized(this)
        {
            registeredThreads.remove(thread);
        }
    }

    public void registerAndRemoveThreads(HashMap<Thread, Prediction> registerUpdate, ArrayList<Thread> threads)
    {
        synchronized(this)
        {
            registeredThreads.putAll(registerUpdate);

            for(Thread thread : threads)
            {
                registeredThreads.remove(thread);
            }
        }
    }

    public Object sharedAccess(Thread thread, ArrayList<Thread> ancestors, boolean inWhile, String methodName, Object... varargs)
    {
        Prediction predictionContext = null;

        boolean sharedAccessAllowed = false;

        Object returnObject = null;

        while(!sharedAccessAllowed)
        {
            synchronized(this)
            {
                predictionContext = getPredictionContext(thread, ancestors);

                sharedAccessAllowed = sharedAccessAllowed(methodName, predictionContext);

                if(sharedAccessAllowed)
                {
                    for(Method method : this.getClass().getDeclaredMethods())
                    {
                        if(methodIsAppropriate(method, methodName, varargs))
                        {
                            try
                            {
                                returnObject = method.invoke(this, varargs);
                            }
                            catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }

                    System.out.print(thread.getName() + " executed " + identifier + "." + methodName + "\n");

                    if(!inWhile)
                    {
                        registeredThreads.get(thread).decrementPrediction(identifier, methodName);
                    }

                    policyAutomaton.doTransition(identifier, methodName);

                    notifyAll();
                }
                else
                {
                    try
                    {
                        System.out.println(Thread.currentThread().getName() + " waits");

                        this.wait();
                    }
                    catch(InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }

        return returnObject;
    }

    private boolean sharedAccessAllowed(String methodName, Prediction predictionContext)
    {
        if(!predictionContext.getMap().isEmpty())
        {
            ArrayList<String> precedingMethods = policyAutomaton.getPrecedingMethods(methodName);

            for(String precedingMethod : precedingMethods)
            {
                if(predictionContext.getMap().get(identifier).get(precedingMethod) != null && predictionContext.getMap().get(identifier).get(precedingMethod).intValue() > 0)
                {
                    System.out.print(Thread.currentThread().getName() + " tried to execute " + identifier + "." + methodName + " but " + identifier + "." + precedingMethod + " = " + predictionContext.getMap().get(identifier).get(precedingMethod).intValue() + " was seen in " + predictionContext.getPrintablePrediction() + "\n");

                    return false;
                }

                if(predictionContext.getMap().get(identifier).get(precedingMethod + ":while") != null && predictionContext.getMap().get(identifier).get(precedingMethod + ":while").intValue() > 0)
                {
                    System.out.print(Thread.currentThread().getName() + " tried to execute " + identifier + "." + methodName + " but " + identifier + "." + precedingMethod + ":while = " + predictionContext.getMap().get(identifier).get(precedingMethod + ":while").intValue() + " was seen in " + predictionContext.getPrintablePrediction() + "\n");

                    return false;
                }
            }
        }

        return true;
    }

    private boolean methodIsAppropriate(Method method, String methodName, Object... varargs)
    {
        if(!method.getName().equals(methodName))
        {
            return false;
        }

        if(method.getParameterTypes().length != varargs.length)
        {
            return false;
        }

        for(int index = 0; index < varargs.length; index++)
        {
            if(!typeIsAppropriate(method.getParameterTypes()[index], varargs[index]))
            {
                return false;
            }
        }

        return true;
    }

    private boolean typeIsAppropriate(Class<?> parameterType, Object object)
    {
        if(object == null)
        {
            return !parameterType.isPrimitive();
        }

        if(parameterType.isInstance(object))
        {
            return true;
        }

        if(parameterType.isPrimitive())
        {
            try
            {
                return !object.getClass().isPrimitive() && object.getClass().getDeclaredField("TYPE").get(null).equals(parameterType);
            }
            catch(NoSuchFieldException e)
            {
                return false;
            }
            catch(IllegalArgumentException | IllegalAccessException | SecurityException e)
            {
                e.printStackTrace();
            }
        }

        return false;
    }

    private Prediction getPredictionContext(Thread thread, ArrayList<Thread> ancestors)
    {
        Prediction predictionContext = new Prediction();

        for(Entry<Thread, Prediction> entry : registeredThreads.entrySet())
        {
            if(!thread.equals(entry.getKey()) && !ancestors.contains(entry.getKey()))
            {
                predictionContext.addPrediction(entry.getValue());
            }
        }

        return predictionContext;
    }

    private PolicyAutomaton getPolicyAutomaton()
    {
        HashMap<String, HashMap<String, String>> transitions = new HashMap<String, HashMap<String, String>>();
        MethodAutomaton methodAutomaton = null;
        String initialState = null;

        if(this.getClass().isAnnotationPresent(MethodAutomaton.class))
        {
            methodAutomaton = this.getClass().getAnnotation(MethodAutomaton.class);

            initialState = methodAutomaton.initialState();

            if(initialState == null)
            {
                System.err.print("The initial state of the " + MethodAutomaton.class.getName() + " annotation found in class " + this.getClass().getName() + " is null\n");

                System.exit(0);
            }

            String[] transitionSplit = null;

            for(String transition : methodAutomaton.transitions())
            {
                if(transition == null)
                {
                    System.err.print("One of the transitions of the " + MethodAutomaton.class.getName() + " annotation in class " + this.getClass().getName() + " is null\n");

                    System.exit(0);
                }

                transitionSplit = transition.split("\\.");

                if(transitionSplit.length != 3)
                {
                    System.err.print("Wrong transition format in \"" + transition + "\"\n");

                    System.exit(0);
                }

                if(transitions.get(transitionSplit[0]) == null)
                {
                    transitions.put(transitionSplit[0], new HashMap<String, String>());
                }

                if(transitions.get(transitionSplit[0]).get(transitionSplit[1]) != null)
                {
                    if(transitions.get(transitionSplit[0]).get(transitionSplit[1]).equals(transitionSplit[2]))
                    {
                        System.err.print("Duplicate transition found: \"" + transition + "\"\n");
                    }
                    else
                    {
                        System.err.print("Non-deterministic transitions found: \"" + transitionSplit[0] + "." + transitionSplit[1] + "." + transitions.get(transitionSplit[0]).get(transitionSplit[1]) + "\", \"" + transition + "\"\n");
                    }

                    System.exit(0);
                }

                transitions.get(transitionSplit[0]).put(transitionSplit[1], transitionSplit[2]);
            }
        }
        else
        {
            System.err.print("No " + MethodAutomaton.class.getName() + " annotation found in class " + this.getClass().getName() + "\n");

            System.exit(0);
        }

        HashMap<String, HashMap<String, ArrayList<String>>> statefulPrecedences = new HashMap<String, HashMap<String, ArrayList<String>>>();
        StatefulPrecedingMethods statefulPrecedingMethods = null;
        int precededMethodCount = 0;

        for(Method method : this.getClass().getDeclaredMethods())
        {
            if(method.isAnnotationPresent(StatefulPrecedingMethods.class))
            {
                precededMethodCount++;

                statefulPrecedingMethods = method.getAnnotation(StatefulPrecedingMethods.class);

                if(statefulPrecedingMethods.states().length != statefulPrecedingMethods.methods().length)
                {
                    System.err.print("Unequal number of states and method lists in method annotation for " + method.getName() + "\n");

                    System.exit(0);
                }

                for(int index = 0; index < statefulPrecedingMethods.states().length; index++)
                {
                    if(statefulPrecedences.get(statefulPrecedingMethods.states()[index]) == null)
                    {
                        statefulPrecedences.put(statefulPrecedingMethods.states()[index], new HashMap<String, ArrayList<String>>());
                    }

                    if(statefulPrecedences.get(statefulPrecedingMethods.states()[index]).get(method.getName()) != null)
                    {
                        System.err.print("Duplicate state found: \"" + statefulPrecedingMethods.states()[index] + "\"\n");

                        System.exit(0);
                    }

                    statefulPrecedences.get(statefulPrecedingMethods.states()[index]).put(method.getName(), new ArrayList<String>(Arrays.asList(statefulPrecedingMethods.methods()[index].split("\\."))));
                }
            }
        }

        for(Entry<String, HashMap<String, String>> transition : transitions.entrySet())
        {
            if(transition.getValue().size() != precededMethodCount)
            {
                if(transition.getValue().size() < precededMethodCount)
                {
                    System.err.print("The method automaton of " + this.getClass().getName() + " is not complete. There are " + transition.getValue().size() + " transitions on state " + transition.getKey() + " but " + precededMethodCount + " preceded methods defined.\n");
                }
                else
                {
                    System.err.print("The method automaton of " + this.getClass().getName() + " has too many transitions. There are " + transition.getValue().size() + " transitions on state " + transition.getKey() + " but " + precededMethodCount + " preceded methods defined\n");
                }

                System.exit(0);
            }
        }

        return new PolicyAutomaton(initialState, transitions, statefulPrecedences);
    }

    /**
     * The value of a clock-synchronized object is reset after each tick. Use this method to do so.
     */
    public abstract void resetValue();

    public void reset()
    {
        synchronized(this)
        {
            resetValue();

            policyAutomaton.reset();
        }
    }
}