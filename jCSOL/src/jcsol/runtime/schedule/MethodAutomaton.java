package jcsol.runtime.schedule;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(TYPE)
public @interface MethodAutomaton
{
    public String initialState() default "0";

    public String[] transitions() default {};
}