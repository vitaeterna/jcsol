package jcsol.runtime.schedule;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Task implements Runnable
{
    private ArrayList<ClockSynchronizedObject> registeredCSOs = null;
    private ArrayList<Thread>                  ancestors      = null;
    private int                                id             = -1;
    private AtomicInteger                      childrenAlive  = new AtomicInteger(2);

    public Task(ArrayList<Thread> ancestors, int id)
    {
        registeredCSOs = new ArrayList<ClockSynchronizedObject>();

        this.ancestors = ancestors;
        this.id = id;
    }

    public void addCSO(ClockSynchronizedObject cso)
    {
        registeredCSOs.add(cso);
    }

    public void removeCSO(ClockSynchronizedObject cso)
    {
        registeredCSOs.remove(cso);
    }

    public ArrayList<ClockSynchronizedObject> getRegisteredCSOs()
    {
        return registeredCSOs;
    }

    public ArrayList<Thread> cloneAndAddAncestor(Thread thread)
    {
        ArrayList<Thread> clone = new ArrayList<Thread>(ancestors);

        clone.add(thread);

        return clone;
    }

    public Thread getDirectAncestor()
    {
        return ancestors.get(ancestors.size() - 1);
    }

    public ArrayList<Thread> getAncestors()
    {
        return ancestors;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public AtomicInteger getChildrenAlive()
    {
        return childrenAlive;
    }

    public void resetChildrenAlive()
    {
        childrenAlive.set(2);
    }

    @Override
    public void run()
    {

    }
}