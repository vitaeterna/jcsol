package jcsol.runtime.schedule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import jcsol.data.ControlFlowGraphNode;
import jcsol.data.Prediction;

public class Register
{
    private static HashMap<String, ClockSynchronizedObject> csoMap            = new HashMap<String, ClockSynchronizedObject>();
    private static HashMap<Thread, Task>                    pauseMap          = new HashMap<Thread, Task>();

    private static AtomicInteger                            activeThreadCount = new AtomicInteger(0);
    private static AtomicInteger                            pauseCount        = new AtomicInteger(0);

    private static ArrayList<ControlFlowGraphNode>          orderedVertices   = null;

    public static void setOrderedVertices(ArrayList<ControlFlowGraphNode> orderedVertices)
    {
        Register.orderedVertices = orderedVertices;
    }

    public static void addCSO(String key, ClockSynchronizedObject value)
    {
        csoMap.put(key, value);
    }

    public static void setPrediction(Thread thread, Task task, int id)
    {
        System.out.print(thread.getName() + " reached cfg update node " + id + " | Prediction Update:\n");

        Prediction prediction = null;

        if(id < 0)
        {
            prediction = new Prediction();
        }
        else
        {
            prediction = orderedVertices.get(id).getPrediction();
        }

        if(prediction == null)
        {
            System.err.print(thread.getName() + " prediction is null\n");

            System.exit(0);
        }
        else
        {
            Prediction predictionUpdate = null;
            HashMap<Thread, Prediction> registerUpdate = null;

            for(Entry<String, ClockSynchronizedObject> entry : csoMap.entrySet())
            {
                registerUpdate = new HashMap<Thread, Prediction>();

                if(task.getRegisteredCSOs().contains(entry.getValue()))
                {
                    if(prediction.isEmpty(entry.getKey()))
                    {
                        //Remove thread
                        entry.getValue().removeThread(thread);
                        task.getRegisteredCSOs().remove(entry.getValue());
                    }
                    else
                    {
                        //Update thread
                        predictionUpdate = entry.getValue().getEmptyPrediction().addPrediction(prediction.deepCopyPrediction(entry.getKey()));
                        registerUpdate.put(thread, predictionUpdate);
                        entry.getValue().registerThreads(registerUpdate);
                    }
                }
                else if(!prediction.isEmpty(entry.getKey()))
                {
                    //Add thread
                    predictionUpdate = entry.getValue().getEmptyPrediction().addPrediction(prediction.deepCopyPrediction(entry.getKey()));
                    registerUpdate.put(thread, predictionUpdate);
                    entry.getValue().registerThreads(registerUpdate);
                    task.getRegisteredCSOs().add(entry.getValue());
                }
            }

            System.out.print(thread.getName() + " " + prediction.getPrintablePrediction() + "\n");
        }
    }

    private static void setPredictions(Thread thread0, Thread thread1, Thread thread2, Task task0, Task task1, Task task2, int id0)
    {
        System.out.print(thread0.getName() + " forks " + thread1.getName() + " and " + thread2.getName() + " | Prediction Updates:\n");

        Prediction prediction0 = id0 == -1 ? new Prediction() : orderedVertices.get(id0).getPrediction();
        Prediction prediction1 = task1.getId() == -1 ? new Prediction() : orderedVertices.get(task1.getId()).getPrediction();
        Prediction prediction2 = task2.getId() == -1 ? new Prediction() : orderedVertices.get(task2.getId()).getPrediction();

        if(prediction0 == null)
        {
            System.err.print(thread0.getName() + " prediction is null\n");

            System.exit(0);
        }
        else if(prediction1 == null)
        {
            System.err.print(thread1.getName() + " prediction is null\n");

            System.exit(0);
        }
        else if(prediction2 == null)
        {
            System.err.print(thread2.getName() + " prediction is null\n");

            System.exit(0);
        }
        else
        {
            Prediction predictionUpdate = null;
            HashMap<Thread, Prediction> registerUpdate = null;
            ArrayList<Thread> threads = null;

            Thread thread = null;
            Task task = null;
            Prediction prediction = null;

            for(Entry<String, ClockSynchronizedObject> entry : csoMap.entrySet())
            {
                registerUpdate = new HashMap<Thread, Prediction>();
                threads = new ArrayList<Thread>();

                for(int index = 0; index < 3; index++)
                {
                    if(index == 0)
                    {
                        thread = thread0;
                        task = task0;
                        prediction = prediction0;
                    }
                    else if(index == 1)
                    {
                        thread = thread1;
                        task = task1;
                        prediction = prediction1;
                    }
                    else if(index == 2)
                    {
                        thread = thread2;
                        task = task2;
                        prediction = prediction2;
                    }

                    if(task.getRegisteredCSOs().contains(entry.getValue()))
                    {
                        if(prediction.isEmpty(entry.getKey()))
                        {
                            //Remove thread
                            threads.add(thread);
                            task.getRegisteredCSOs().remove(entry.getValue());
                        }
                        else
                        {
                            //Update thread
                            predictionUpdate = entry.getValue().getEmptyPrediction().addPrediction(prediction.deepCopyPrediction(entry.getKey()));
                            registerUpdate.put(thread, predictionUpdate);
                        }
                    }
                    else if(!prediction.isEmpty(entry.getKey()))
                    {
                        //Add thread
                        predictionUpdate = entry.getValue().getEmptyPrediction().addPrediction(prediction.deepCopyPrediction(entry.getKey()));
                        registerUpdate.put(thread, predictionUpdate);
                        task.getRegisteredCSOs().add(entry.getValue());
                    }
                }

                if(!registerUpdate.isEmpty() || !threads.isEmpty())
                {
                    entry.getValue().registerAndRemoveThreads(registerUpdate, threads);
                }
            }

            System.out.print(thread0.getName() + " " + prediction0.getPrintablePrediction() + "\n");
            System.out.print(thread1.getName() + " " + prediction1.getPrintablePrediction() + "\n");
            System.out.print(thread2.getName() + " " + prediction2.getPrintablePrediction() + "\n");
        }
    }

    public static void createThread(Thread thread, Task task)
    {
        Register.setPrediction(thread, task, task.getId());

        activeThreadCount.incrementAndGet();

        thread.start();
    }

    public static void createThreads(Thread thread0, Thread thread1, Thread thread2, Task task0, Task task1, Task task2, int id0)
    {
        Register.setPredictions(thread0, thread1, thread2, task0, task1, task2, id0);

        task0.resetChildrenAlive();

        activeThreadCount.incrementAndGet();

        thread1.start();
        thread2.start();
    }

    public static void joinThread(Thread thread)
    {
        try
        {
            thread.join();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }

        activeThreadCount.decrementAndGet();
    }

    public static void joinThreads(Thread thread0, Thread thread1)
    {
        try
        {
            thread0.join();
            thread1.join();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public static void pause(Thread thread, Task task, int id)
    {
        Register.setPrediction(thread, task, -1);

        //Build up global pause storage
        task.setId(id);

        synchronized(pauseMap)
        {
            pauseMap.put(thread, task);
        }

        boolean allOthersArePaused = false;

        synchronized(activeThreadCount)
        {
            synchronized(pauseCount)
            {
                allOthersArePaused = activeThreadCount.intValue() == pauseCount.intValue() + 1;

                if(allOthersArePaused)
                {
                    tick();
                }
                else
                {
                    pauseCount.incrementAndGet();
                }
            }
        }

        if(!allOthersArePaused)
        {
            //Pause
            synchronized(pauseMap)
            {
                try
                {
                    System.out.print(Thread.currentThread().getName() + " pauses\n");

                    pauseMap.wait();
                }
                catch(InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void tick()
    {
        for(ClockSynchronizedObject value : csoMap.values())
        {
            value.reset();
        }

        synchronized(pauseMap)
        {
            pauseCount = new AtomicInteger(0);

            for(Entry<Thread, Task> entry : pauseMap.entrySet())
            {
                setPrediction(entry.getKey(), entry.getValue(), entry.getValue().getId());
            }

            pauseMap.clear();

            System.out.print(Thread.currentThread().getName() + " notifies all pausing threads\n");

            pauseMap.notifyAll();
        }
    }

    public static void notifyTermination(Thread thread, Task task, AtomicInteger childrenAlive)
    {
        Register.setPrediction(thread, task, -1);

        synchronized(childrenAlive)
        {
            if(childrenAlive.decrementAndGet() == 1)
            {
                synchronized(activeThreadCount)
                {
                    synchronized(pauseCount)
                    {
                        activeThreadCount.decrementAndGet();

                        if(activeThreadCount.intValue() == pauseCount.intValue())
                        {
                            tick();
                        }
                    }
                }
            }
        }

        System.out.print(Thread.currentThread().getName() + " notifies termination\n");
    }

    public static void terminate()
    {
        System.out.print(Thread.currentThread().getName() + " terminated\n");

        System.exit(0);
    }
}