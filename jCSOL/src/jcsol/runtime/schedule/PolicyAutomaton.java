package jcsol.runtime.schedule;

import java.util.ArrayList;
import java.util.HashMap;

public class PolicyAutomaton
{
    private String                                              state               = null;
    private String                                              initialState        = null;
    private HashMap<String, HashMap<String, String>>            transitions         = null;
    private HashMap<String, HashMap<String, ArrayList<String>>> statefulPrecedences = null;

    public PolicyAutomaton(String initialState, HashMap<String, HashMap<String, String>> transitions, HashMap<String, HashMap<String, ArrayList<String>>> statefulPrecedences)
    {
        this.initialState = initialState;
        this.transitions = transitions;
        this.statefulPrecedences = statefulPrecedences;

        reset();
    }

    public ArrayList<String> getPrecedingMethods(String methodName)
    {
        if(statefulPrecedences.get(state) != null && statefulPrecedences.get(state).get(methodName) != null)
        {
            return statefulPrecedences.get(state).get(methodName);
        }

        return new ArrayList<String>();
    }

    public void doTransition(String identifier, String methodName)
    {
        if(transitions.get(state) != null && transitions.get(state).get(methodName) != null)
        {
            state = transitions.get(state).get(methodName);
        }
        else
        {
            System.err.print("Method " + identifier + "." + methodName + " not applicable in state \"" + state + "\" since there is no transition " + state + "." + methodName + ".?" + "\n");

            System.exit(0);
        }
    }

    /**
     * The policy automaton of a clock-synchronized object is reset after each tick.
     */
    public void reset()
    {
        state = initialState;
    }
}