package jcsol.runtime.launch;

import java.io.IOException;
import java.nio.charset.Charset;

import jcsol.data.ControlFlowGraph;
import jcsol.runtime.generated.GeneratedCode;
import jcsol.runtime.schedule.Register;
import jcsol.util.Util;

public class Launcher
{
    public static void main(String[] args)
    {
        if(args.length != 1)
        {
            System.err.print("Only 1 argument is allowed. " + args.length + " arguments found.\n");

            System.exit(0);
        }

        Util.charsetUTF8();

        Thread terminateOnParentDeath = new Thread()
        {
            @Override
            public void run()
            {
                int input = 0;

                try
                {
                    while(input != -1)
                    {
                        input = System.in.read();
                    }
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }

                System.out.print("Terminating csol process.\n");

                System.exit(0);
            }
        };

        terminateOnParentDeath.start();

        String json = Util.readFile(args[0], Charset.defaultCharset());

        ControlFlowGraph cfg = Util.fromJson(json, ControlFlowGraph.class);

        Register.setOrderedVertices(cfg.getVertices());

        if(cfg.verticesAreOrdered())
        {
            GeneratedCode code = new GeneratedCode();

            code.execute();
        }
    }
}