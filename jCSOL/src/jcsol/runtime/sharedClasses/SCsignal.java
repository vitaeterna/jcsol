package jcsol.runtime.sharedClasses;

import jcsol.runtime.schedule.ClockSynchronizedObject;
import jcsol.runtime.schedule.MethodAutomaton;
import jcsol.runtime.schedule.StatefulPrecedingMethods;

@MethodAutomaton(initialState = "0", transitions = { "0.present.0", "0.emit.1", "1.present.1", "1.emit.1" })
public class SCsignal extends ClockSynchronizedObject
{
    private boolean value = false;

    @StatefulPrecedingMethods(states = { "0" }, methods = { "emit" })
    public boolean present()
    {
        return value;
    }

    @StatefulPrecedingMethods()
    public void emit()
    {
        value = true;
    }

    @Override
    public void resetValue()
    {
        value = false;
    }
}