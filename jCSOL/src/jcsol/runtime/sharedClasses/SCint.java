package jcsol.runtime.sharedClasses;

import jcsol.runtime.schedule.ClockSynchronizedObject;
import jcsol.runtime.schedule.MethodAutomaton;
import jcsol.runtime.schedule.StatefulPrecedingMethods;

@MethodAutomaton(initialState = "0", transitions = { "0.i.0", "0.decrement.0", "0.r.0" })
public class SCint extends ClockSynchronizedObject
{
    private Integer value = null;

    @StatefulPrecedingMethods(states = { "0" }, methods = { "i" })
    public void i(int value)
    {
        this.value = value;
    }

    @StatefulPrecedingMethods(states = { "0" }, methods = { "i" })
    public void decrement()
    {
        value--;
    }

    @StatefulPrecedingMethods(states = { "0" }, methods = { "i.decrement" })
    public int r()
    {
        return value.intValue();
    }

    @Override
    public void resetValue()
    {
        value = null;
    }
}