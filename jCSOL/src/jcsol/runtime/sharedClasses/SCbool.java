package jcsol.runtime.sharedClasses;

import jcsol.runtime.schedule.ClockSynchronizedObject;
import jcsol.runtime.schedule.MethodAutomaton;
import jcsol.runtime.schedule.StatefulPrecedingMethods;

@MethodAutomaton(initialState = "0", transitions = { "0.i.0", "0.u.0", "0.r.0" })
public class SCbool extends ClockSynchronizedObject
{
    private Boolean value = null;

    @StatefulPrecedingMethods(states = { "0" }, methods = { "i" })
    public void i(boolean value)
    {
        this.value = value;
    }

    @StatefulPrecedingMethods(states = { "0" }, methods = { "i" })
    public void u(boolean value)
    {
        this.value = this.value.booleanValue() | value;
    }

    @StatefulPrecedingMethods(states = { "0" }, methods = { "i.u" })
    public boolean r()
    {
        return value.booleanValue();
    }

    @Override
    public void resetValue()
    {
        value = null;
    }
}