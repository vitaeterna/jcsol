package jcsol.data;

import java.util.HashMap;
import java.util.Map.Entry;

public class Prediction
{
    private HashMap<String, HashMap<String, Integer>> prediction = null;

    public Prediction()
    {
        prediction = new HashMap<String, HashMap<String, Integer>>();
    }

    private Prediction(HashMap<String, HashMap<String, Integer>> prediction)
    {
        this.prediction = prediction;
    }

    public void incrementPrediction(String key0, String key1, boolean inWhile)
    {
        if(prediction.get(key0) == null)
        {
            prediction.put(key0, new HashMap<String, Integer>());

            if(inWhile)
            {
                prediction.get(key0).put(key1 + ":while", 1);
            }
            else
            {
                prediction.get(key0).put(key1, 1);
            }
        }
        else
        {
            if(inWhile)
            {
                if(prediction.get(key0).get(key1 + ":while") == null)
                {
                    prediction.get(key0).put(key1 + ":while", 1);
                }
                else
                {
                    prediction.get(key0).put(key1 + ":while", prediction.get(key0).get(key1 + ":while").intValue() + 1);
                }
            }
            else
            {
                if(prediction.get(key0).get(key1) == null)
                {
                    prediction.get(key0).put(key1, 1);
                }
                else
                {
                    prediction.get(key0).put(key1, prediction.get(key0).get(key1).intValue() + 1);
                }
            }
        }
    }

    public void decrementPrediction(String key0, String key1)
    {
        if(prediction.get(key0) != null)
        {
            if(prediction.get(key0).get(key1) != null)
            {
                prediction.get(key0).put(key1, prediction.get(key0).get(key1).intValue() - 1);

                if(prediction.get(key0).get(key1) < 0)
                {
                    System.err.print(Thread.currentThread().getName() + " " + key0 + "." + key1 + " is negative\n");

                    System.exit(0);
                }
            }
            else
            {
                System.err.print(Thread.currentThread().getName() + " " + key1 + " not found\n");

                System.exit(0);
            }
        }
        else
        {
            System.err.print(Thread.currentThread().getName() + " " + key0 + " not found\n");

            System.exit(0);
        }

        System.out.print(Thread.currentThread().getName() + " " + getPrintablePrediction() + "\n");
    }

    public String getPrintablePrediction()
    {
        StringBuilder result = new StringBuilder();

        result.append("Prediction\n{\n");

        for(Entry<String, HashMap<String, Integer>> entry0 : prediction.entrySet())
        {
            for(Entry<String, Integer> entry1 : entry0.getValue().entrySet())
            {
                if(entry1.getValue().intValue() != 0)
                {
                    result.append("    " + entry0.getKey() + "." + entry1.getKey() + " = " + entry1.getValue().intValue() + "\n");
                }
            }
        }

        result.append("}");

        return result.toString();
    }

    public Prediction deepCopyPrediction()
    {
        HashMap<String, HashMap<String, Integer>> clone = new HashMap<String, HashMap<String, Integer>>();

        for(Entry<String, HashMap<String, Integer>> entry0 : prediction.entrySet())
        {
            clone.put(entry0.getKey(), new HashMap<String, Integer>());

            for(Entry<String, Integer> entry1 : entry0.getValue().entrySet())
            {
                clone.get(entry0.getKey()).put(entry1.getKey(), entry1.getValue());
            }
        }

        return new Prediction(clone);
    }

    public Prediction deepCopyPrediction(String key)
    {
        HashMap<String, HashMap<String, Integer>> clone = new HashMap<String, HashMap<String, Integer>>();

        for(Entry<String, HashMap<String, Integer>> entry0 : prediction.entrySet())
        {
            if(entry0.getKey().equals(key))
            {
                clone.put(entry0.getKey(), new HashMap<String, Integer>());

                for(Entry<String, Integer> entry1 : entry0.getValue().entrySet())
                {
                    clone.get(entry0.getKey()).put(entry1.getKey(), entry1.getValue());
                }
            }
        }

        return new Prediction(clone);
    }

    public Prediction addPrediction(Prediction prediction)
    {
        for(Entry<String, HashMap<String, Integer>> entry0 : prediction.getMap().entrySet())
        {
            if(this.prediction.get(entry0.getKey()) == null)
            {
                this.prediction.put(entry0.getKey(), new HashMap<String, Integer>());
            }

            for(Entry<String, Integer> entry1 : entry0.getValue().entrySet())
            {
                if(this.prediction.get(entry0.getKey()).get(entry1.getKey()) == null)
                {
                    this.prediction.get(entry0.getKey()).put(entry1.getKey(), entry1.getValue());
                }
                else
                {
                    this.prediction.get(entry0.getKey()).put(entry1.getKey(), this.prediction.get(entry0.getKey()).get(entry1.getKey()) + entry1.getValue());
                }
            }
        }

        return this;
    }

    public boolean isEmpty(String key)
    {
        if(prediction.get(key) == null || prediction.get(key).isEmpty())
        {
            return true;
        }
        else
        {
            for(Integer integer : prediction.get(key).values())
            {
                if(integer.intValue() > 0)
                {
                    return false;
                }
            }
        }

        return true;
    }

    public HashMap<String, HashMap<String, Integer>> getMap()
    {
        return prediction;
    }
}