package jcsol.data;

import java.util.ArrayList;

public class ControlFlowGraphNode
{
    public static enum Type
    {
        START, STOP, SKIP, PAUSE, DECLARATION, CALL, ASSIGN, CONDITION, CEND, WHILE, WEND, FORK, JOIN;
    }

    private static int        nextId      = 0;

    private int               id          = -1;
    private Type              type        = null;
    private String            value       = null;
    private boolean           inWhile     = false;
    private int               group       = -1;

    //START, PAUSE, CEND, WEND, JOIN, first in CONDITION branch, first in FORK branch
    private Prediction        prediction  = null;

    //CEND, WEND, JOIN
    private int               sourceId    = 0;
    private int               branchId    = 0;

    //DECLARATION, CALL
    private ArrayList<String> identifiers = null;
    private String            scClass     = null;

    public ControlFlowGraphNode(Type type)
    {
        id = nextId++;

        this.type = type;

        identifiers = new ArrayList<String>();
    }

    public ControlFlowGraphNode(Type type, int group)
    {
        id = nextId++;

        this.type = type;

        this.group = group;

        identifiers = new ArrayList<String>();
    }

    public ControlFlowGraphNode(Type type, String value, boolean inWhile, int group)
    {
        id = nextId++;

        this.type = type;

        this.value = value;

        this.inWhile = inWhile;

        this.group = group;

        identifiers = new ArrayList<String>();
    }

    public int getId()
    {
        return id;
    }

    public Type getType()
    {
        return type;
    }

    public String getValue()
    {
        return value;
    }

    public boolean inWhile()
    {
        return inWhile;
    }

    public int getGroup()
    {
        return group;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public void setInWhile(boolean inWhile)
    {
        this.inWhile = inWhile;
    }

    public void setIdentifier(String identifier)
    {
        identifiers.add(identifier);
    }

    public void addIdentifier(String identifier)
    {
        identifiers.add(identifier);
    }

    public String getIdentifier()
    {
        return identifiers.get(0);
    }

    public ArrayList<String> getIdentifiers()
    {
        return identifiers;
    }

    public void setMethod(String method)
    {
        identifiers.add(method);
    }

    public String getMethod()
    {
        return identifiers.get(1);
    }

    public void setSCClass(String scClass)
    {
        this.scClass = scClass;
    }

    public String getSCClass()
    {
        return scClass;
    }

    public void setPrediction(Prediction prediction)
    {
        this.prediction = prediction;
    }

    public Prediction getPrediction()
    {
        return prediction;
    }

    public void setSourceId(int sourceId)
    {
        this.sourceId = sourceId;
    }

    public int getSourceId()
    {
        return sourceId;
    }

    public void setBranchId(int branchId)
    {
        this.branchId = branchId;
    }

    public int getBranchId()
    {
        return branchId;
    }

    public static int getNextId()
    {
        return nextId;
    }

    public static void resetIdCounter()
    {
        nextId = 0;
    }

    @Override
    public String toString()
    {
        return String.valueOf(id);
    }
}