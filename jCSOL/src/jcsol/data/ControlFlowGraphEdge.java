package jcsol.data;

public class ControlFlowGraphEdge
{
    private int    id0   = 0;
    private int    id1   = 0;
    private String label = null;

    public ControlFlowGraphEdge(int id0, int id1, String label)
    {
        this.id0 = id0;
        this.id1 = id1;
        this.label = label;
    }

    public int getId0()
    {
        return id0;
    }

    public int getId1()
    {
        return id1;
    }

    public String getLabel()
    {
        return label;
    }

    @Override
    public String toString()
    {
        return getId0() + " " + getId1() + " " + getLabel();
    }
}