package jcsol.data;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import jcsol.data.ControlFlowGraphNode.Type;
import jcsol.runtime.schedule.StatefulPrecedingMethods;
import jcsol.util.Util;

public class ControlFlowGraph
{
    //For Runtime
    private ArrayList<ControlFlowGraphNode>           vertices                = null;
    private ArrayList<ControlFlowGraphEdge>           edges                   = null;

    //For Visualization and algorithms
    private transient ArrayList<ControlFlowGraphEdge> whileEdges              = null;
    private transient ArrayList<ControlFlowGraphEdge> invisibleWhileEdges     = null;
    private transient ArrayList<ControlFlowGraphEdge> precedenceEdges         = null;
    private transient ArrayList<ControlFlowGraphEdge> invisibleFillerEdges    = null;
    private transient ArrayList<ControlFlowGraphNode> invisibleFillerVertices = null;
    private transient ArrayList<ArrayList<Integer>>   fillerRanks             = null;
    private transient ArrayList<ArrayList<Integer>>   ranks                   = null;

    public ControlFlowGraph(ArrayList<ControlFlowGraphNode> vertices, ArrayList<ControlFlowGraphEdge> edges)
    {
        this.vertices = vertices;
        this.edges = edges;
    }

    public void setVertices(ArrayList<ControlFlowGraphNode> vertices)
    {
        this.vertices = vertices;
    }

    public void setEdges(ArrayList<ControlFlowGraphEdge> edges)
    {
        this.edges = edges;
    }

    public void setPrecedenceEdges(ArrayList<ControlFlowGraphEdge> precedenceEdges)
    {
        this.precedenceEdges = precedenceEdges;
    }

    public void setWhileEdges(ArrayList<ControlFlowGraphEdge> whileEdges)
    {
        this.whileEdges = whileEdges;
    }

    public void setInvisibleWhileEdges(ArrayList<ControlFlowGraphEdge> invisibleWhileEdges)
    {
        this.invisibleWhileEdges = invisibleWhileEdges;
    }

    public void setInvisibleFillerEdges(ArrayList<ControlFlowGraphEdge> invisibleFillerEdges)
    {
        this.invisibleFillerEdges = invisibleFillerEdges;
    }

    public void setInvisibleFillerVertices(ArrayList<ControlFlowGraphNode> invisibleFillerVertices)
    {
        this.invisibleFillerVertices = invisibleFillerVertices;
    }

    public void setFillerRanks(ArrayList<ArrayList<Integer>> fillerRanks)
    {
        this.fillerRanks = fillerRanks;
    }

    public void setRanks(ArrayList<ArrayList<Integer>> ranks)
    {
        this.ranks = ranks;
    }

    public ArrayList<ControlFlowGraphNode> getVertices()
    {
        return vertices;
    }

    public ArrayList<ControlFlowGraphEdge> getEdges()
    {
        return edges;
    }

    public ArrayList<ControlFlowGraphEdge> getPrecedenceEdges()
    {
        return precedenceEdges;
    }

    public ArrayList<ControlFlowGraphEdge> getWhileEdges()
    {
        return whileEdges;
    }

    public ArrayList<ControlFlowGraphEdge> getInvisibleWhileEdges()
    {
        return invisibleWhileEdges;
    }

    public ArrayList<ControlFlowGraphEdge> getInvisibleFillerEdges()
    {
        return invisibleFillerEdges;
    }

    public ArrayList<ControlFlowGraphNode> getInvisibleFillerVertices()
    {
        return invisibleFillerVertices;
    }

    public ArrayList<ArrayList<Integer>> getFillerRanks()
    {
        return fillerRanks;
    }

    public ArrayList<ArrayList<Integer>> getRanks()
    {
        return ranks;
    }

    public boolean verticesAreOrdered()
    {
        for(int index = 0; index < vertices.size(); index++)
        {
            if(index != vertices.get(index).getId())
            {
                return false;
            }
        }

        return true;
    }

    public boolean containsEdge(int id0, int id1)
    {
        for(ControlFlowGraphEdge edge : edges)
        {
            if(edge.getId0() == id0 && edge.getId1() == id1)
            {
                return true;
            }
        }

        return false;
    }

    public ArrayList<ControlFlowGraphNode> getSuccessors(int id)
    {
        ArrayList<ControlFlowGraphNode> successors = new ArrayList<ControlFlowGraphNode>();

        for(ControlFlowGraphEdge edge : edges)
        {
            if(vertices.get(id) == vertices.get(edge.getId0()) && !successors.contains(vertices.get(edge.getId1())))
            {
                successors.add(vertices.get(edge.getId1()));
            }
        }

        return successors;
    }

    public ArrayList<ControlFlowGraphNode> getPredecessors(int id)
    {
        ArrayList<ControlFlowGraphNode> predecessors = new ArrayList<ControlFlowGraphNode>();

        for(ControlFlowGraphEdge edge : edges)
        {
            if(vertices.get(id) == vertices.get(edge.getId1()) && !predecessors.contains(vertices.get(edge.getId0())))
            {
                predecessors.add(vertices.get(edge.getId0()));
            }
        }

        return predecessors;
    }

    public ArrayList<ControlFlowGraphNode> getReflexiveNodes()
    {
        ArrayList<ControlFlowGraphNode> reflexives = new ArrayList<ControlFlowGraphNode>();

        for(ControlFlowGraphEdge edge : edges)
        {
            if(edge.getId0() == edge.getId1())
            {
                reflexives.add(vertices.get(edge.getId0()));
            }
        }

        return reflexives;
    }

    public void analyze()
    {
        if(!allWhilesGuarded())
        {
            System.err.println("Not all whiles guarded!");

            System.exit(0);
        }

        calculatePredictions();
        calculatePrecedenceEdges();
        calculateInvisiblesAndRanks();
    }

    public boolean allWhilesGuarded()
    {
        boolean allWhilesGuarded = true;

        linearizeWhiles();

        for(int index = 0; index < vertices.size(); index++)
        {
            if(vertices.get(index).getType().equals(Type.WEND))
            {
                allWhilesGuarded &= whileGuarded(vertices.get(index));
            }
        }

        delinearizeWhiles();

        return allWhilesGuarded;
    }

    private void linearizeWhiles()
    {
        edges.removeAll(whileEdges);
        edges.addAll(invisibleWhileEdges);
    }

    private void delinearizeWhiles()
    {
        edges.addAll(whileEdges);
        edges.removeAll(invisibleWhileEdges);
    }

    private boolean whileGuarded(ControlFlowGraphNode wendNode)
    {
        ControlFlowGraphNode finalNode = vertices.get(wendNode.getSourceId());

        System.out.print("while(" + finalNode.getId() + ":" + wendNode.getId() + ") ");

        ArrayList<ControlFlowGraphNode> visited = new ArrayList<ControlFlowGraphNode>();
        ArrayList<ControlFlowGraphNode> todo = new ArrayList<ControlFlowGraphNode>();

        todo.add(wendNode);

        for(int index = 0; index < todo.size(); index++)
        {
            wendNode = todo.get(index);

            if(visited.contains(wendNode))
            {
                continue;
            }
            else
            {
                visited.add(wendNode);
            }

            if(wendNode.equals(finalNode))
            {
                System.err.println("not clock guarded!");

                return false;
            }

            if(!wendNode.getType().equals(Type.PAUSE))
            {
                todo.addAll(getPredecessors(todo.get(index).getId()));
            }
        }

        System.out.println("clock guarded!");

        return true;
    }

    private void calculatePrecedenceEdges()
    {
        ArrayList<String> scClasses = new ArrayList<String>();

        for(ControlFlowGraphNode vertex0 : vertices)
        {
            if(vertex0.getType().equals(Type.DECLARATION))
            {
                if(!scClasses.contains(vertex0.getSCClass()))
                {
                    scClasses.add(vertex0.getSCClass());
                }

                for(ControlFlowGraphNode vertex1 : vertices)
                {
                    if(vertex1.getType().equals(Type.CALL))
                    {
                        if(vertex0.getIdentifiers().contains(vertex1.getIdentifier()))
                        {
                            vertex1.setSCClass(vertex0.getSCClass());
                        }
                    }
                }
            }
        }

        HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>> statefulPrecedences = new HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>();

        StatefulPrecedingMethods statefulPrecedingMethods = null;

        for(String scClass : scClasses)
        {
            statefulPrecedences.put(scClass, new HashMap<String, HashMap<String, ArrayList<String>>>());

            try
            {
                for(Method method : Class.forName("jcsol.runtime.sharedClasses." + scClass).getDeclaredMethods())
                {
                    if(method.isAnnotationPresent(StatefulPrecedingMethods.class))
                    {
                        statefulPrecedingMethods = method.getAnnotation(StatefulPrecedingMethods.class);

                        for(int index = 0; index < statefulPrecedingMethods.states().length; index++)
                        {
                            if(statefulPrecedences.get(scClass).get(statefulPrecedingMethods.states()[index]) == null)
                            {
                                statefulPrecedences.get(scClass).put(statefulPrecedingMethods.states()[index], new HashMap<String, ArrayList<String>>());
                            }

                            if(statefulPrecedences.get(scClass).get(statefulPrecedingMethods.states()[index]).get(method.getName()) != null)
                            {
                                System.err.print("Duplicate state found: \"" + statefulPrecedingMethods.states()[index] + "\"\n");

                                System.exit(0);
                            }

                            statefulPrecedences.get(scClass).get(statefulPrecedingMethods.states()[index]).put(method.getName(), new ArrayList<String>(Arrays.asList(statefulPrecedingMethods.methods()[index].split("\\."))));
                        }
                    }
                }
            }
            catch(ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }

        precedenceEdges = new ArrayList<ControlFlowGraphEdge>();

        for(ControlFlowGraphNode vertex0 : vertices)
        {
            if(vertex0.getType().equals(Type.CALL))
            {
                for(ControlFlowGraphNode vertex1 : vertices)
                {
                    if(vertex1.getType().equals(Type.CALL))
                    {
                        if(vertex0.getSCClass().equals(vertex1.getSCClass()) && vertex0.getIdentifier().equals(vertex1.getIdentifier()))
                        {
                            for(Entry<String, HashMap<String, ArrayList<String>>> entry : statefulPrecedences.get(vertex0.getSCClass()).entrySet())
                            {
                                if(entry.getValue().get(vertex0.getMethod()).contains(vertex1.getMethod()))
                                {
                                    precedenceEdges.add(new ControlFlowGraphEdge(vertex1.getId(), vertex0.getId(), entry.getKey()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //TODO: are while edges important here?
    private void calculateInvisiblesAndRanks()
    {
        invisibleFillerVertices = new ArrayList<ControlFlowGraphNode>();
        invisibleFillerEdges = new ArrayList<ControlFlowGraphEdge>();
        fillerRanks = new ArrayList<ArrayList<Integer>>();
        ranks = new ArrayList<ArrayList<Integer>>();

        ArrayList<ControlFlowGraphNode> sameRankedVertices = new ArrayList<ControlFlowGraphNode>();
        ArrayList<ControlFlowGraphNode> tempRankedVertices = new ArrayList<ControlFlowGraphNode>();
        ArrayList<ControlFlowGraphNode> visited = new ArrayList<ControlFlowGraphNode>();
        int[] joinCounters = new int[vertices.size()];
        ArrayList<ArrayList<ControlFlowGraphNode>> forkStack = new ArrayList<ArrayList<ControlFlowGraphNode>>();
        ArrayList<ControlFlowGraphNode> tempFork = new ArrayList<ControlFlowGraphNode>();

        sameRankedVertices.add(vertices.get(0));

        while(!sameRankedVertices.isEmpty())
        {
            ArrayList<Integer> tempList = new ArrayList<Integer>();

            for(ControlFlowGraphNode vertex : sameRankedVertices)
            {
                tempList.add(vertex.getId());
            }

            fillerRanks.add(tempList);

            tempRankedVertices = new ArrayList<ControlFlowGraphNode>();

            for(ControlFlowGraphNode vertex : sameRankedVertices)
            {
                if(vertex.getType() == null)
                {
                    continue;
                }

                visited.add(vertex);

                if(vertex.getType().equals(Type.WHILE) || vertex.getType().equals(Type.CONDITION) || vertex.getType().equals(Type.FORK))
                {
                    tempFork = new ArrayList<ControlFlowGraphNode>();
                    tempFork.add(vertex);
                    forkStack.add(tempFork);
                }

                for(ControlFlowGraphEdge edge : edges)
                {
                    if(edge.getId0() == vertex.getId() && !visited.contains(vertices.get(edge.getId1())) && !tempRankedVertices.contains(vertices.get(edge.getId1())))
                    {
                        if(vertices.get(edge.getId1()).getSourceId() != 0)
                        {
                            joinCounters[edge.getId1()]++;

                            if(joinCounters[edge.getId1()] == 2)
                            {
                                tempRankedVertices.add(vertices.get(edge.getId1()));
                            }
                        }
                        else
                        {
                            tempRankedVertices.add(vertices.get(edge.getId1()));
                        }
                    }
                }
            }

            for(ControlFlowGraphNode vertex : tempRankedVertices)
            {
                if(vertex.getType().equals(Type.WEND) || vertex.getType().equals(Type.CEND) || vertex.getType().equals(Type.JOIN))
                {
                    for(ArrayList<ControlFlowGraphNode> fork : forkStack)
                    {
                        if(fork.get(0).getId() == vertex.getSourceId())
                        {
                            invisibleFillerEdges.add(new ControlFlowGraphEdge(fork.get(fork.size() - 1).getId(), vertex.getId(), null));

                            forkStack.remove(fork);

                            break;
                        }
                    }
                }
            }

            for(ArrayList<ControlFlowGraphNode> fork : forkStack)
            {
                ControlFlowGraphNode tempVertex = new ControlFlowGraphNode(null, fork.get(fork.size() - 1).getGroup());

                invisibleFillerEdges.add(new ControlFlowGraphEdge(fork.get(fork.size() - 1).getId(), tempVertex.getId(), null));

                fork.add(tempVertex);

                invisibleFillerVertices.add(tempVertex);

                tempRankedVertices.add(tempVertex);
            }

            sameRankedVertices = tempRankedVertices;
        }

        ArrayList<Integer> rank = null;

        for(ArrayList<Integer> fillerRank : fillerRanks)
        {
            rank = new ArrayList<Integer>();

            for(Integer integer : fillerRank)
            {
                if(integer < invisibleFillerVertices.get(0).getId())
                {
                    rank.add(integer);
                }
            }

            ranks.add(rank);
        }
    }

    public void calculatePredictions()
    {
        for(int index = 0; index < vertices.size(); index++)
        {
            if(vertices.get(index).getType().equals(Type.CALL))
            {
                propagateSharedAccess(vertices.get(index));
            }
        }
    }

    //While jumps missing because they do not exist yet, do it when cfg is finished!
    private void propagateSharedAccess(ControlFlowGraphNode callNode)
    {
        System.out.print("\npropagate " + callNode.getIdentifier() + "." + callNode.getMethod() + "\n");

        ArrayList<ControlFlowGraphNode> visited = new ArrayList<ControlFlowGraphNode>();
        ArrayList<ControlFlowGraphNode> todo = new ArrayList<ControlFlowGraphNode>();
        ArrayList<ControlFlowGraphNode> predecessors = null;

        todo.add(callNode);

        for(int index = 0; index < todo.size(); index++)
        {
            callNode = todo.get(index);

            if(visited.contains(callNode))
            {
                continue;
            }
            else
            {
                visited.add(callNode);
            }

            System.out.print("node " + callNode.getId());

            if(callNode.getPrediction() != null)
            {
                callNode.getPrediction().incrementPrediction(callNode.getIdentifier(), callNode.getMethod(), false);

                System.out.print(" updated");
            }

            if(callNode.getType().equals(Type.JOIN))
            {
                todo.add(vertices.get(callNode.getSourceId()));

                System.out.print(" -> " + callNode.getSourceId());
            }
            else if(!callNode.getType().equals(Type.PAUSE))
            {
                predecessors = getPredecessors(callNode.getId());

                todo.addAll(predecessors);

                System.out.print(" -> " + predecessors);
            }

            System.out.print("\n");
        }
    }

    @Override
    public String toString()
    {
        return Util.toJson(this);
    }
}