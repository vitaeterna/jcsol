package jcsol.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jcsol.data.ControlFlowGraph;
import jcsol.data.ControlFlowGraphEdge;
import jcsol.data.ControlFlowGraphNode;

public class Util
{
    public static String readFile(String path, Charset encoding)
    {
        byte[] encoded = null;

        try
        {
            encoded = Files.readAllBytes(Paths.get(path));
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

        return new String(encoded, encoding);
    }

    public static void writeFile(String path, String content, Charset encoding)
    {
        byte[] encoded = content.getBytes(encoding);

        try
        {
            Files.write(new File(path).toPath(), encoded);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static boolean deleteDirectory(File dir)
    {
        if(dir.exists())
        {
            if(dir.isDirectory())
            {
                String[] children = dir.list();

                for(int i = 0; i < children.length; i++)
                {
                    if(!deleteDirectory(new File(dir, children[i])))
                    {
                        return false;
                    }
                }
            }

            return dir.delete();
        }
        else
        {
            return true;
        }
    }

    public static boolean createDirectory(File dir)
    {
        return dir.mkdirs();
    }

    public static String toJson(Object object)
    {
        return (new GsonBuilder().setPrettyPrinting().create()).toJson(object);
    }

    public static <T> T fromJson(String json, Class<T> classT)
    {
        return (new Gson()).fromJson(json, classT);
    }

    public static void charsetUTF8()
    {
        if(!Charset.defaultCharset().name().equals("UTF-8"))
        {
            System.err.print("Default charset is not UTF-8 but " + Charset.defaultCharset().name() + "\n");
            System.err.print("Use VM argument -Dfile.encoding=UTF-8\n");

            System.exit(0);
        }
        else
        {
            System.out.print("Charset: " + Charset.defaultCharset().name() + "\n\n");
        }
    }

    public static void graphDotExport(String outputFilename, ControlFlowGraph cfg, boolean useFillerNodes, boolean drawPrecedences)
    {
        StringBuilder dot = new StringBuilder();

        dot.append("digraph CFG\n");
        dot.append("{\n");
        dot.append("    graph [outputorder=edgesfirst, splines=true, dpi=300, fontname=\"Courier New Bold\"];\n");
        dot.append("\n    node [style=filled, fillcolor=white, color=black, fontname=\"Courier New Bold\"];\n");

        //CFG Nodes
        for(ControlFlowGraphNode vertex : cfg.getVertices())
        {
            if(vertex.getType().equals(ControlFlowGraphNode.Type.START))
            {
                dot.append("    " + vertex.getId() + " [group=g" + vertex.getGroup() + ", label=\"" + vertex.getId() + "_" + vertex.getType() + "\", shape=circle, width=1];\n");
            }
            else if(vertex.getType().equals(ControlFlowGraphNode.Type.STOP))
            {
                dot.append("    " + vertex.getId() + " [group=g" + vertex.getGroup() + ", label=\"" + vertex.getId() + "_" + vertex.getType() + "\", shape=doublecircle, width=1];\n");
            }
            else if(vertex.getType().equals(ControlFlowGraphNode.Type.CONDITION) || vertex.getType().equals(ControlFlowGraphNode.Type.WHILE))
            {
                dot.append("    " + vertex.getId() + " [group=g" + vertex.getGroup() + ", label=\"" + vertex.getId() + "_" + vertex.getType() + "\\n" + vertex.getValue() + "\", shape=diamond, width=3];\n");
            }
            else if(vertex.getType().equals(ControlFlowGraphNode.Type.CEND) || vertex.getType().equals(ControlFlowGraphNode.Type.WEND))
            {
                dot.append("    " + vertex.getId() + " [group=g" + vertex.getGroup() + ", label=\"" + vertex.getId() + "_" + vertex.getType() + "\", shape=octagon, width=3];\n");
            }
            else if(vertex.getType().equals(ControlFlowGraphNode.Type.FORK))
            {
                dot.append("    " + vertex.getId() + " [group=g" + vertex.getGroup() + ", label=\"" + vertex.getId() + "_" + vertex.getType() + "\", shape=triangle, width=3];\n");
            }
            else if(vertex.getType().equals(ControlFlowGraphNode.Type.JOIN))
            {
                dot.append("    " + vertex.getId() + " [group=g" + vertex.getGroup() + ", label=\"" + vertex.getId() + "_" + vertex.getType() + "\", shape=invtriangle, width=3];\n");
            }
            else if(vertex.getType().equals(ControlFlowGraphNode.Type.SKIP) || vertex.getType().equals(ControlFlowGraphNode.Type.PAUSE))
            {
                dot.append("    " + vertex.getId() + " [group=g" + vertex.getGroup() + ", label=\"" + vertex.getId() + "_" + vertex.getType() + "\", shape=box, width=2];\n");
            } //DECLARATION, ASSIGN, CALL
            else
            {
                dot.append("    " + vertex.getId() + " [group=g" + vertex.getGroup() + ", label=\"" + vertex.getId() + "_" + vertex.getType() + "\\n" + vertex.getValue() + "\", shape=box, width=2];\n");
            }
        }

        ArrayList<ArrayList<Integer>> ranks = null;

        if(useFillerNodes)
        {
            //FillerNodes
            for(ControlFlowGraphNode vertex : cfg.getInvisibleFillerVertices())
            {
                dot.append("    " + vertex.getId() + " [group=g" + vertex.getGroup() + ", shape=circle, width=1, fixedsize=true];\n");
            }

            //Ranks with fillers
            ranks = cfg.getFillerRanks();
        }
        else
        {
            //Ranks without fillers
            ranks = cfg.getRanks();
        }

        for(ArrayList<Integer> sameRank : ranks)
        {
            dot.append("    { rank=same; ");

            for(int index1 = 0; index1 < sameRank.size(); index1++)
            {
                dot.append(sameRank.get(index1));

                if(index1 != sameRank.size() - 1)
                {
                    dot.append(",");
                }

                dot.append(" ");
            }

            dot.append("}\n");
        }

        dot.append("\n    edge [arrowhead=vee, arrowtail=none, color=black, fontname=\"Courier New Bold\", weight=1];\n");

        ArrayList<ControlFlowGraphNode> reflexive = cfg.getReflexiveNodes();

        //CFG Edges
        for(ControlFlowGraphEdge edge : cfg.getEdges())
        {
            if(edge.getLabel() == null)
            {
                if(cfg.getVertices().get(edge.getId1()).getType().equals(ControlFlowGraphNode.Type.CEND) || cfg.getVertices().get(edge.getId1()).getType().equals(ControlFlowGraphNode.Type.JOIN))
                {
                    if(cfg.getVertices().get(edge.getId0()).getBranchId() == 1)
                    {
                        dot.append("    " + edge.getId0() + ":s -> " + edge.getId1() + ":nw;\n");
                    }
                    else if(cfg.getVertices().get(edge.getId0()).getBranchId() == 2)
                    {
                        dot.append("    " + edge.getId0() + ":s -> " + edge.getId1() + ":ne;\n");
                    }
                    else
                    {
                        dot.append("    " + edge.getId0() + ":s -> " + edge.getId1() + ":n;\n");
                    }
                }
                else
                {
                    dot.append("    " + edge.getId0() + ":s -> " + edge.getId1() + ":n;\n");
                }
            }
            else if(edge.getLabel().equals("jump"))
            {
                dot.append("    " + edge.getId0() + ":w -> " + edge.getId1() + ":nw;\n");
            }
            else if(edge.getLabel().equals("true;jump"))
            {
                dot.append("    " + edge.getId0() + ":sw -> " + edge.getId1() + ":nw [label=\"true\"];\n");
            }
            else if(edge.getLabel().equals("true"))
            {
                if(cfg.getVertices().get(edge.getId1()).getSourceId() == edge.getId0())
                {
                    dot.append("    " + edge.getId0() + ":sw -> " + edge.getId1() + ":nw [label=\"" + edge.getLabel() + "\"];\n");
                }
                else
                {
                    dot.append("    " + edge.getId0() + ":sw -> " + edge.getId1() + ":n [label=\"" + edge.getLabel() + "\"];\n");
                }
            }
            else if(edge.getLabel().equals("false"))
            {
                if(cfg.getVertices().get(edge.getId1()).getSourceId() == edge.getId0())
                {
                    if(reflexive.contains(cfg.getVertices().get(edge.getId0())))
                    {
                        dot.append("    " + edge.getId0() + ":s -> " + edge.getId1() + ":n [label=\"" + edge.getLabel() + "\"];\n");
                    }
                    else
                    {
                        dot.append("    " + edge.getId0() + ":se -> " + edge.getId1() + ":ne [label=\"" + edge.getLabel() + "\"];\n");
                    }
                }
                else
                {
                    dot.append("    " + edge.getId0() + ":se -> " + edge.getId1() + ":n [label=\"" + edge.getLabel() + "\"];\n");
                }
            }
            else if(edge.getLabel().equals("1"))
            {
                if(cfg.getVertices().get(edge.getId1()).getSourceId() == edge.getId0())
                {
                    dot.append("    " + edge.getId0() + ":sw -> " + edge.getId1() + ":nw;\n");
                }
                else
                {
                    dot.append("    " + edge.getId0() + ":sw -> " + edge.getId1() + ":n;\n");
                }
            }
            else if(edge.getLabel().equals("2"))
            {
                if(cfg.getVertices().get(edge.getId1()).getSourceId() == edge.getId0())
                {
                    dot.append("    " + edge.getId0() + ":se -> " + edge.getId1() + ":ne;\n");
                }
                else
                {
                    dot.append("    " + edge.getId0() + ":se -> " + edge.getId1() + ":n;\n");
                }
            }
        }

        dot.append("\n    edge [arrowhead=none, arrowtail=none, color=black, fontname=\"Courier New Bold\", weight=10];\n");

        if(useFillerNodes)
        {
            //WHILE->WEND CONDITION->CEND FORK->JOIN connected via filler nodes
            for(ControlFlowGraphEdge edge : cfg.getInvisibleFillerEdges())
            {
                dot.append("    " + edge.getId0() + ":s -> " + edge.getId1() + ":n [style=\"dotted\"];\n");
            }
        }
        else
        {
            //WHILE->WEND CONDITION->CEND FORK->JOIN connected directly
            for(ControlFlowGraphNode vertex : cfg.getVertices())
            {
                if(vertex.getType().equals(ControlFlowGraphNode.Type.CEND) || vertex.getType().equals(ControlFlowGraphNode.Type.JOIN))
                {
                    dot.append("    " + cfg.getVertices().get(vertex.getSourceId()).getId() + ":s -> " + vertex.getId() + ":n [style=\"dotted\"];\n");
                }
                else if(vertex.getType().equals(ControlFlowGraphNode.Type.WEND))
                {
                    if(!reflexive.contains(cfg.getVertices().get(vertex.getSourceId())))
                    {
                        dot.append("    " + cfg.getVertices().get(vertex.getSourceId()).getId() + ":s -> " + vertex.getId() + ":n [style=\"dotted\"];\n");
                    }
                }
            }
        }

        //Edges from last while statement to WEND which is usually not there
        for(ControlFlowGraphEdge edge : cfg.getInvisibleWhileEdges())
        {
            if(!reflexive.contains(cfg.getVertices().get(cfg.getVertices().get(edge.getId1()).getSourceId())))
            {
                dot.append("    " + edge.getId0() + ":s -> " + edge.getId1() + ":n [style=\"invis\"];\n");
            }
        }

        if(drawPrecedences)
        {
            dot.append("\n    edge [arrowhead=vee, arrowtail=none, color=red, fontname=\"Courier New Bold\", fontcolor=red, weight=0];\n");

            //Precedence edges
            for(ControlFlowGraphEdge edge : cfg.getPrecedenceEdges())
            {
                dot.append("    " + edge.getId0() + " -> " + edge.getId1() + " [label=\"" + edge.getLabel() + "\", style=\"dotted\", weight=0];\n");
            }
        }

        dot.append("}");

        Util.writeFile(outputFilename + ".dot", dot.toString(), Charset.defaultCharset());
    }
}