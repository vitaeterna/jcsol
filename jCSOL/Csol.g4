grammar Csol;

@header
{
    package jcsol.compiler.antlr4;
}

@members
{
    java.util.HashMap<String, jcsol.compiler.Declaration> variables = new java.util.HashMap<String, jcsol.compiler.Declaration>();

    int layer = -1;
    
    String variable  = null;
    String methodName = null;
    
    int param = 0;

    private void layerIncrement()
    {
    	layer++;
    }

    private void layerDecrement()
    {
    	layer--;
    	
		java.util.Iterator iterator = variables.entrySet().iterator();
		
		while(iterator.hasNext())
		{
	        java.util.HashMap.Entry entry = (java.util.HashMap.Entry)iterator.next();
	        
	        if(((jcsol.compiler.Declaration)entry.getValue()).getLayer() > layer)
	        {
	        	iterator.remove();
	        }
		}
    }

    private void dec(String variable, String type, int line, int pos)
    {
    	if(variables.containsKey(variable))
    	{
    		System.err.print("Duplicate variable \"" + variable + "\" (" + line + ":" + pos + ")\n");
    		
    		System.exit(0);
    	}
    	else
    	{
    		variables.put(variable, new jcsol.compiler.Declaration(variable, type == null ? null : type.toLowerCase(), layer));
    	}
    }

    private void use(String variable, String type, int line, int pos)
    {
    	if(!variables.containsKey(variable))
    	{
    		System.err.print("\"" + variable + "\" cannot be resolved to a variable (" + line + ":" + pos + ")\n");
    		
    		System.exit(0);
    	}
    	else
    	{
    		if(type != null)
    		{
    			if(!variables.get(variable).getType().equals(type))
    			{
    				System.err.print("Type mismatch: found " + variables.get(variable).getType() + " (type of " + variable + "), expected " + type + " (" + line + ":" + pos + ")\n");
    			
    			    System.exit(0);
    			}
    		}
    		else
    		{
    			this.variable = variable;
    		}
    	}
    }
    
    private void updateDecType(String type)
    {
    	java.util.Iterator iterator = variables.entrySet().iterator();
		
		while(iterator.hasNext())
		{
	        java.util.HashMap.Entry entry = (java.util.HashMap.Entry)iterator.next();
	        
	        if(((jcsol.compiler.Declaration)entry.getValue()).getType() == null)
	        {
	        	((jcsol.compiler.Declaration)entry.getValue()).setType(type);
	        }
		}
    }
    
    private void resetParam(String methodName)
    {
    	param = 0;
    	
    	this.methodName = methodName;
    }
    
    private boolean paramTypeIs(String type)
    {
        try
        {
            for(java.lang.reflect.Method method : Class.forName("jcsol.runtime.sharedClasses." + variables.get(variable).getType()).getDeclaredMethods())
            {
                if(method.getName().equals(methodName))
                {
                    return method.getParameterTypes()[param].getName().equals(type);
                }
            }
        }
        catch(SecurityException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    	
    	System.err.print(variables.get(variable).getType() + " " + variable + "." + methodName + "[" + param + ":" + type + "] not found");
    	
    	System.exit(0);
    	
    	return false;
    }
}

//Lexer Rules

CSOL             : ('c'|'C')('s'|'S')('o'|'O')('l'|'L');
MODULE           : ('m'|'M')('o'|'O')('d'|'D')('u'|'U')('l'|'L')('e'|'E');
NEW              : ('n'|'N')('e'|'E')('w'|'W');
VOID             : ('v'|'V')('o'|'O')('i'|'I')('d'|'D');
INT              : ('i'|'I')('n'|'N')('t'|'T');
BOOL             : ('b'|'B')('o'|'O')('o'|'O')('l'|'L');
SKIPP            : ('s'|'S')('k'|'K')('i'|'I')('p'|'P');
PAUSE            : ('p'|'P')('a'|'A')('u'|'U')('s'|'S')('e'|'E');
IF               : ('i'|'I')('f'|'F');
ELSE             : ('e'|'E')('l'|'L')('s'|'S')('e'|'E');
WHILE            : ('w'|'W')('h'|'H')('i'|'I')('l'|'L')('e'|'E');
FORK             : ('f'|'F')('o'|'O')('r'|'R')('k'|'K');
JAVA             : ('j'|'J')('a'|'A')('v'|'V')('a'|'A')(' '|'\t'|'\f'|'\r'|'\n')*('{'.*?'}');
BOOLEAN          : ('t'|'T')('r'|'R')('u'|'U')('e'|'E')|('f'|'F')('a'|'A')('l'|'L')('s'|'S')('e'|'E');
INTEGER          : '0'|([1-9]DIGIT*);
IDENTIFIER       : (LETTER|'_')(LETTER|'_'|DIGIT)*;
ASSIGN           : '=';
COMMA            : ',';
DOT              : '.';
SEQUENTIAL       : ';';
LEFTBRACE        : '{';
RIGHTBRACE       : '}';
LEFTBRACKET      : '[';
RIGHTBRACKET     : ']';
LEFTPARENTHESIS  : '(';
RIGHTPARENTHESIS : ')';
PLUS             : '+';
MINUS            : '-';
MULTIPLY         : '*';
DIVIDE           : '/';
MODULO           : '%';
NOT              : '!';
BITAND           : '&';
BITOR            : '|';
LOGAND           : '&&';
LOGOR            : '||';
EQUAL            : '==';
NOTEQUAL         : '!=';
LESSTHAN         : '<';
GREATERTHAN      : '>';
LESSOREQUAL      : '<=';
GREATEROREQUAL   : '>=';
SINGLEQUOTES     : '\'';
DOUBLEQUOTES     : '"';
WHITESPACE       : (' '|'\t'|'\f'|'\r'|'\n')+    -> skip;
COMMENT          : (('//'.*?'\n')|('/*'.*?'*/')) -> skip;
fragment DIGIT   : [0-9];
fragment LETTER  : [a-zA-Z];

//Parser Rules

csolProgram  : CSOL MODULE IDENTIFIER type IDENTIFIER LEFTPARENTHESIS RIGHTPARENTHESIS LEFTBRACE { layerIncrement(); } statement1 { layerDecrement(); } RIGHTBRACE EOF;
statement1   : statement2 SEQUENTIAL statement1
             | statement3 statement1
             | /* epsilon */;
statement2   : skip
             | pause
             | declaration
             | methodCall
             | assignment;
skip         : SKIPP;
pause        : PAUSE;
declaration  : IDENTIFIER { dec($IDENTIFIER.text, null, $IDENTIFIER.line, $IDENTIFIER.pos); } multipleDec ASSIGN NEW IDENTIFIER { updateDecType($IDENTIFIER.text); };
multipleDec  : COMMA IDENTIFIER { dec($IDENTIFIER.text, null, $IDENTIFIER.line, $IDENTIFIER.pos); } multipleDec
             | /* epsilon */;
assignment   : type IDENTIFIER { dec($IDENTIFIER.text, $type.text , $IDENTIFIER.line, $IDENTIFIER.pos); } ASSIGN methodCall;
type         : VOID
             | INT
             | BOOL;
methodCall   : IDENTIFIER { use($IDENTIFIER.text, null, $IDENTIFIER.line, $IDENTIFIER.pos); } DOT IDENTIFIER LEFTPARENTHESIS { resetParam($IDENTIFIER.text); } parameters RIGHTPARENTHESIS;
parameters   : { paramTypeIs("int") }?     expressionI1 { param++; } multiplePar
             | { paramTypeIs("boolean") }? expressionB1 { param++; } multiplePar
             | /* epsilon */;
multiplePar  : { paramTypeIs("int") }?     COMMA expressionI1 { param++; } multiplePar
             | { paramTypeIs("boolean") }? COMMA expressionB1 { param++; } multiplePar
             | /* epsilon */;
statement3   : condition
             | loop
             | fork
             | java;
condition    : IF LEFTPARENTHESIS expressionB1 RIGHTPARENTHESIS LEFTBRACE { layerIncrement(); } ifstmt { layerDecrement(); } RIGHTBRACE
             | IF LEFTPARENTHESIS expressionB1 RIGHTPARENTHESIS LEFTBRACE { layerIncrement(); } ifstmt { layerDecrement(); } RIGHTBRACE ELSE LEFTBRACE { layerIncrement(); } ifstmt { layerDecrement(); } RIGHTBRACE;
ifstmt       : statement1;
loop         : WHILE LEFTPARENTHESIS expressionB1 RIGHTPARENTHESIS LEFTBRACE { layerIncrement(); } loopstmt { layerDecrement(); } RIGHTBRACE;
loopstmt     : statement1;
fork         : LEFTBRACE { layerIncrement(); } forkstmt { layerDecrement(); } RIGHTBRACE FORK LEFTBRACE { layerIncrement(); } forkstmt { layerDecrement(); } RIGHTBRACE;
forkstmt     : statement1;
java         : JAVA;
expressionB1 : expressionI1 EQUAL          expressionI1
             | expressionI1 NOTEQUAL       expressionI1
             | expressionI1 LESSTHAN       expressionI1
             | expressionI1 GREATERTHAN    expressionI1
             | expressionI1 LESSOREQUAL    expressionI1
             | expressionI1 GREATEROREQUAL expressionI1
             | expressionB2 EQUAL          expressionB1
             | expressionB2 NOTEQUAL       expressionB1
             | expressionB2;
expressionB2 : expressionB3 LOGOR expressionB2
             | expressionB3;
expressionB3 : expressionB4 LOGAND expressionB3
             | expressionB4;
expressionB4 : expressionB5 BITOR expressionB4
             | expressionB5;
expressionB5 : expressionB6 BITAND expressionB5
             | expressionB6;
expressionB6 : LEFTPARENTHESIS expressionB1 RIGHTPARENTHESIS
             | NOT expressionB1
             | IDENTIFIER { use($IDENTIFIER.text, "bool", $IDENTIFIER.line, $IDENTIFIER.pos); }
             | BOOLEAN;
expressionI1 : expressionI2 PLUS  expressionI1
             | expressionI2 MINUS expressionI1
             | expressionI2;
expressionI2 : expressionI3 MULTIPLY expressionI2
             | expressionI3 DIVIDE   expressionI2
             | expressionI3 MODULO   expressionI2
             | expressionI3;
expressionI3 : LEFTPARENTHESIS expressionI1 RIGHTPARENTHESIS
             | MINUS expressionI1
             | IDENTIFIER { use($IDENTIFIER.text, "int", $IDENTIFIER.line, $IDENTIFIER.pos); }
             | INTEGER;