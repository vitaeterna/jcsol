# jCSOL - java implementation of the clock-synchronized objects language #

### How do I get set up? ###

1. Download and install jdk 8 or higher

2. Download eclipse for java developers 4.1 or higher

3. Install "ANTLR 4 IDE" as eclipse plugin from the eclipse marketplace

4. Import the eclipse project from this repository

    1. File -> Import -> Git -> Projects from Git -> Next -> Clone URI -> Next

    2. Copy "https://bitbucket.org/vitaeterna/jcsol" to the URI field -> Next* -> Finish

5. Configure the antlr path

    1. Window -> Preferences -> ANTLR 4 -> Tool -> Configure Project Specific Settings...

    2. Select the "jCSOL" project -> Ok

    3. Add -> Select the antlr complete jar in the lib folder of the project

    4. Check the added path -> Apply and Close

6. The main method of the jcsol.compiler.Compiler class is the entry point

    1. Just open jcsol.compiler.Compiler in eclipse and press CTRL + F11

    2. Ignore the errors and proceed

        1. If you get an error that the default charset is not UTF-8

        2. Right click on the project -> Run As -> Run Configurations -> Compiler -> Arguments -> VM arguments

        3. Add -Dfile.encoding=UTF-8 as vm argument -> Apply -> Run -> Proceed

    3. The jcsol.runtime.generated.GeneratedCode class was generated

    4. Click on the project and then F5 to refresh the files and eclipse recognises the generated file

Don't be surprised that there is no jcsol.runtime.generated.GeneratedCode class file. It is generated from the .csol source file by the compiler

You can specify the .csol source file which shall be compiled and run in jcsol.compiler.Compiler.inputFilenameCSOL

### Graph visualization ###

The jcsol compiler gernerates a .dot file representing the control flow graph of the compiled csol program

If a dot renderer is available via the path variable a .png file for the control flow graph is generated too

### Todos from Future Work (Master's Thesis) ###

 - Remove rudimentary create-thread-handshake (DONE)
 - Ensure thread-safety for pause/tick (DONE)
 - Reset policy automaton and clock-synchronized object values on tick (DONE)
 - Implement input/output system
 - Fix errors in prediction calculation for while loops
 - Handle non-concurrent admissibility (DONE)